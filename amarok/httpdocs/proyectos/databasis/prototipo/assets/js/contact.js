

$(document).ready(function(){
    
    //     $("#con_form2").validate({
          
    //       rules         : {
    //           con_name2      : { required : true, minlength: 2},
    //           con_email2     : { required : true, email    : true},
    //           con_mensaje2: { required:true, minlength: 2}
    //       },
    //       messages      : {
    //           con_name2      : "Debe introducir un nombre o empresa.",
    //           con_email2     : "Debe introducir un email válido.",
    //           con_mensaje2 : "El campo Comentario es obligatorio."
              
    //       }
    //   });
    
    // funcion para mostrar el gif enviando
    function muestragif() {

        $('#gif').show();
      }
      function ocultagif() {
          $('#gif').hide();
      }

    $(document).on('submit', '#form_contacto', function(event){
      event.preventDefault();
      muestragif();
    //si la validacion tiene mensajes de error
    var errores = $("#form_contacto").find('.has-error').text();
    
    if(errores != '')
    {
        
        swal(
            'Error!',
            '<h4>Complete todos los campos</h4>',
            'error'
            );
        ocultagif();
        return;
    }
      var form_data = $(this).serialize();
      console.log(form_data)
      $.ajax({
          url: '../../controllers/controllerContacto.php',
          type: 'POST',
          data: form_data,
          success: function(data)
          {
            ocultagif();
            var parsedata = $.parseJSON(data);
            if (parsedata.respuesta == 'ok'){
                $('#form_contacto')[0].reset();
                swal(
                'Datos del Email envíados!',
                '<h4>'+parsedata.message_plano+'</h4>',
                'success'
                );
                //   $('#respuesta').fadeIn(3000).html(parsedata.message).delay(6000).fadeOut(3000);
                
                $('#btn_enviar').attr('disabled', false);
                $('.div_btn_enviar').show(3000);
            } 
            else {
                $('#form_contacto')[0].reset();
                swal(
                'Error!',
                '<h4>'+parsedata.message_plano+'</h4>',
                'error'
                );
                //   $('#respuesta').fadeIn(3000).html(parsedata.message).delay(6000).fadeOut(6000);
                
                $('#btn_enviar').attr('disabled', false);
                $('.div_btn_enviar').show(3000);
                }
            } 
      });
      
      });
    
    
      
    });