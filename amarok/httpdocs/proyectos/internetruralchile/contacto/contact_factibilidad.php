<?php
//index.php
require("../class/class.phpmailer.php"); 
require("../class/class.smtp.php"); 
$message = '';
$name = '';
$rut = '';
$email = '';
$telefono = '';
$mensaje = '';
function clean_text($string)
{
	$string = trim($string);
	$string = stripslashes($string);
	$string = htmlspecialchars($string);
	return $string;
}

$name = $_POST["name"];
$rut = $_POST["rut"];
$email = $_POST["email"];
$telefono = $_POST["telefono"];
$latitud = $_POST["latitud"];
$longitud = $_POST["longitud"];
$mensaje = $_POST["mensaje"];


$email_clear = clean_text($email);
$name_clear = clean_text($name);

if(isset($_POST["g-recaptcha-response"]) && $_POST["g-recaptcha-response"])
{
    $secret = "6LcSR08UAAAAAOg-K0bL30Q-6zSyEfdWa6sQhXUr";
    $ip = $_SERVER["REMOTE_ADDR"];
    $captcha = $_POST["g-recaptcha-response"];
    $result = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $ip);
    $array_result = json_decode($result, TRUE);
    if($array_result['success'])
    {   
        if(isset($email_clear))
        {
        //    $path = 'upload/' . $_FILES["resume"]["name"];
        //    move_uploaded_file($_FILES["resume"]["tmp_name"], $path);
            if(isset($name_clear))
            {
                        
                
                
                $message = '
                    <h3 align="center">Información del Cliente</h3>
                    <table border="1" width="100%" cellpadding="5" cellspacing="5">
                        <tr>
                            <td style="padding-left:10px;" width="30%">Nombre</td>
                            <td style="padding-left:10px;" width="70%">'.$name_clear.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Ruth</td>
                            <td style="padding-left:10px;" width="70%">'.$rut.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Email</td>
                            <td style="padding-left:10px;" width="70%">'.$email_clear.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Télefono</td>
                            <td style="padding-left:10px;" width="70%">'.$telefono.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Latitud</td>
                            <td style="padding-left:10px;" width="70%">'.$latitud.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Longitud</td>
                            <td style="padding-left:10px;" width="70%">'.$longitud.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Mensaje</td>
                            <td style="padding-left:10px;" width="70%">'.$mensaje.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Ip</td>
                            <td style="padding-left:10px;" width="70%">'.$ip.'</td>
                        </tr>
                    </table>
                ';

                $message_muestra = '
                    <h3 align="center">Información del Cliente</h3>
                    <table border="1" width="100%" cellpadding="5" cellspacing="5">
                        <tr>
                            <td style="padding-left:10px;" width="30%">Nombre</td>
                            <td style="padding-left:10px;" width="70%">'.$name_clear.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Rut</td>
                            <td style="padding-left:10px;" width="70%">'.$rut.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Email</td>
                            <td style="padding-left:10px;" width="70%">'.$email_clear.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Télefono</td>
                            <td style="padding-left:10px;" width="70%">'.$telefono.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Latitud</td>
                            <td style="padding-left:10px;" width="70%">'.$latitud.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Longitud</td>
                            <td style="padding-left:10px;" width="70%">'.$longitud.'</td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" width="30%">Mensaje</td>
                            <td style="padding-left:10px;" width="70%">'.$mensaje.'</td>
                        </tr>
                    </table>
                ';


                $mail = new PHPMailer;
                // $mail->IsSMTP();		
                //Sets Mailer to send message using SMTP						
                //Esto es para activar el modo depuración. En entorno de pruebas lo mejor es 2, en producción siempre 0
                // 0 = off (producción)
                // 1 = client messages
                // 2 = client and server messages
                $mail->SMTPDebug  = 0;
                $mail->CharSet = 'UTF-8';
                $mail->Host = 'smtp.gmail.com';
                //Sets the SMTP hosts of your Email hosting, this for gmail	
                //El puerto será el 465 ya que usamos encriptación TLS
                //El puerto 587 es soportado por la mayoría de los servidores SMTP y es útil para conexiones no encriptadas (sin TLS)
                $mail->Port = '465';							//Sets the default SMTP server port
                //Definmos la seguridad como TLS
                $mail->SMTPSecure = 'tls';
                $mail->Username = 'pruebaamarok@gmail.com';		//Sets SMTP username
                $mail->Password = 'amarok123';					//Sets SMTP password
                $mail->SMTPAuth = true;							//Sets SMTP authentication. Utilizes the Username and Password variables

                $mail->From = $email_clear;					//Sets the From email address for the message
                $mail->FromName = $name_clear;				//Sets the From name of the message
                $mail->AddAddress('pruebaamarok@gmail.com', 'Factibilidad');		//Adds a "To" address
                //Para enviar un correo formateado en HTML lo cargamos con la siguiente función. Si no, puedes meterle directamente una cadena de texto.
                $mail->MsgHTML($message);
                //Y por si nos bloquean el contenido HTML (algunos correos lo hacen por seguridad) una versión alternativa en texto plano (también será válida para lectores de pantalla)
                $mail->AltBody = $message;
                $mail->WordWrap = 50;							//Sets word wrapping on the body of the message to a given number of characters
                $mail->IsHTML(true);							//Sets message type to HTML
        //    	$mail->AddAttachment($path);					//Adds an attachment from a path on the filesystem
                $mail->Subject = 'Solicitud de Factibilidad';				//Sets the Subject of the message
                $mail->Body = $message;							//An HTML or plain text message body
                if($mail->Send())								//Send an Email. Return true on success or false on error
                {

                    echo $message = '<div class="alert alert-success text-center">Datos enviados Exitosamente.</div><hr><div class="alert alert-info">'.$message_muestra.'</div>';
        //            unlink($path);
                }
                else
                {
                    echo $message = '<div class="alert alert-danger text-center">Error al intentar enviar los datos</div>';
                }
            }
            else
            {
                echo $message = '<div class="alert alert-danger text-center">Escriba un nombre correcto</div>';
            }
        }
        else
        {
            echo $message = '<div class="alert alert-danger text-center">Escriba un email correcto</div>';
        }
    }
    else
    {
        echo $message = '<div class="alert alert-danger text-center">recaptcha Vacio o Invalido!</div>';
    }    
}
else
{
    echo $message = '<div class="alert alert-danger text-center">Solucione el Recaptcha!</div>';
    
}
?>
