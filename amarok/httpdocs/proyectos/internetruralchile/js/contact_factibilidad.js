$(document).ready(function(){
      
            $("#user_form").validate({
              rules: {
                  name: { required: true, minlength: 2},
                  rut: { required: true, minlength: 2},
                  email: { required:true, email: true},
                  telefono: { minlength: 9, maxlength: 15},
                  latitud: { required: true},
                  longitud: { required: true},
                  mensaje: { required:true, minlength: 2}
              },
              messages: {
                  name: "Debe introducir su nombre.",
                  rut: "Ingrese un rut correcto",
                  email : "Debe introducir un email válido.",
                  telefono : "El número de teléfono introducido no es correcto.",
                  latitud : "Mueva el marcador en el mapa para obtener la Latitud.",
                  longitud : "Mueva el marcador en el mapa para obtener la Longitud.",
                  mensaje : "El campo Mensaje es obligatorio."
              }
          });
//        cuando enviamos la data del formulario de usuario
        $(document).on('submit', '#user_form', function(event){
            event.preventDefault();
            $('#submit').attr('disabled','disabled');
            var form_data = $(this).serialize();

            $.ajax({
             url:"contacto/contact_factibilidad.php",
             type: 'POST',
             data:form_data,
             success:function(data)
             {  
                if(data.length >= 500) {
                $('#user_form')[0].reset();
                swal(
                "Datos del Email envíados!",
                "Gracias pronto te atenderemos!",
                "success"
                );
              $('#alert_action').fadeIn(2000).html(data).delay(6000).fadeOut(3000);
              $('#submit').attr('disabled', false);
             }
             else {
                $('#user_form')[0].reset();
                swal(
                "Error recaptcha!",
                "Intente nuevamente!",
                "error"
                );
                $('#alert_action').fadeIn(2000).html(data).delay(6000).fadeOut(3000);
                // $.getScript("https://www.google.com/recaptcha/api.js");
                $('#submit').attr('disabled', false);
                }
            }  
        });
    });
        
});