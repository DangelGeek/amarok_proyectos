//código para que funcione el marcado de la api de google
var marker;          //variable del marcador
var coords = {};    //coordenadas obtenidas con la geolocalización

//Funcion principal
initMap = function () 
{

    setMapa();  //creamos el mapa        

}


function setMapa ()
{   
      //Se crea una nueva instancia del objeto mapa
      var map = new google.maps.Map(document.getElementById('map'),
      {
        zoom: 13,
        center:new google.maps.LatLng({lat: -41.31669, lng:  -72.98329}),
        mapTypeId: 'hybrid'

      });
      //Mensaje para mostrar en el marker
      var contentString = '<div id="content">'+
            '<h3 id="mueveme" class="mueveme">Mueveme!</h3>'+
            '</div>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString,
              maxWidth: 300
            });

      //Creamos el marcador en el mapa con sus propiedades
      //para nuestro obetivo tenemos que poner el atributo draggable en true
      //position pondremos las mismas coordenas que obtuvimos en la geolocalización
      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng({lat: -41.31669, lng: -72.98329})
      });
      infowindow.open(map, marker);
      //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
      //cuando el usuario a soltado el marcador
      marker.addListener('click', toggleBounce);
      
      marker.addListener( 'dragend', function (event)
      {
        //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
//        document.getElementById("coords").value = this.getPosition().lat()+","+ this.getPosition().lng();
        //escribimos las coordenadas de la posicion actual del marcador dentro del input #latitud
        document.getElementById("latitud").value = this.getPosition().lat();
       
        //escribimos las coordenadas de la posicion actual del marcador dentro del input #longitud
        document.getElementById("longitud").value = this.getPosition().lng();
      });
}

//callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
//funcion para popover al presion en los inputs latitu y longitud
$(function () {
  $('[data-toggle="popover"]').popover()
})
