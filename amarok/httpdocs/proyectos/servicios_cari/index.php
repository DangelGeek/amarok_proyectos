<!DOCTYPE html>
<html class="wide wow-animation" lang="es">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122825359-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122825359-1');
    </script>

    <link rel="alternate" hreflang="es-CL" href="http://www.servicioscari.cl/">
    <link rel="alternate" href="http://www.servicioscari.cl/" hreflang="x-default">
    <title>SERVICIOS CARI | Limpieza de Alfombras y lavado de Tapices en Chile</title>
    <meta name="description" content="Empresa de Limpieza Industrial enfocada en lavado y limpieza de alfombras | lavado de tapices | lavado de sillones | decapado | cualquier parte de Chile.">
 
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width height=device-height initial-scale=1.0 maximum-scale=1.0 user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/lavado_tapiz.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Work+Sans:300,700,800%7COswald:300,400,500">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
      <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/instagram.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/comentarios.css">
    <link rel="stylesheet" href="css/style.css" id="main-styles-link">
    <link rel="stylesheet" href="css/social.css">
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="page">
      <!-- FScreen-->
      <section class="section page-header-navbar rd-navbar-outer jumbotron-classic bg-gray-700 bg-image-dark"> 
        <header class="section page-header">
          <!-- RD Navbar-->
          <div class="rd-navbar-wrap">
            <nav class="rd-navbar rd-navbar-minimal rd-navbar-minimal-wide" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-fixed" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
              <div class="rd-navbar-main-outer">
                <div class="rd-navbar-main">
                  <!-- RD Navbar Panel-->
                  <div class="rd-navbar-panel">
                    <!-- RD Navbar Toggle-->
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                    <!-- RD Navbar Brand-->
                    <a class="rd-navbar-brand" href="index.html">
                       <img src="images/lavado_tapiz.png" alt="Logo de servicioscari" width="111" height="40" srcset="images/lavado_tapiz.png 2x"/><p class="parrafo-logo" >SERVICIOS CARI</p> 
                        
                    </a>
                  </div>
                  <div class="rd-navbar-main-element">
                    <div class="rd-navbar-nav-wrap">
                      <!-- RD Navbar Nav-->
                      <ul class="rd-navbar-nav">
                        <li class="rd-nav-item active"><a class="rd-nav-link" href="index.html">Home</a>
                        </li>
                        <li class="rd-nav-item"><a class="rd-nav-link page-scroll" href="#nosotros">NOSOTROS</a>
                          <!-- RD Navbar Megamenu-->
                          <!-- <ul class="rd-menu rd-navbar-megamenu">
                            <li class="rd-megamenu-item">
                              <ul class="rd-megamenu-list">
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="typography.html">Typography</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="buttons.html">Buttons</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="forms.html">Forms</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="progress-bars.html">Progress bars</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tabs.html">Tabs</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tables.html">Tables</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="accordions.html">Accordions</a></li>
                              </ul>
                            </li>
                            <li class="rd-megamenu-item">
                              <ul class="rd-megamenu-list">
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="blog-layouts.html">Blog Layouts</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="calls-to-action.html">Calls to Action</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="footers.html">Footers</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="maps-and-contacts.html">Maps &amp; Contacts</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="navigation-bars.html">Navigation Bars</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="screens.html">Screens</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="portfolio-and-galleries.html">Portfolio &amp; Galleries</a></li>
                              </ul>
                            </li>
                            <li class="rd-megamenu-item">
                              <ul class="rd-megamenu-list">
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="small-features.html">Small Features</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="team.html">Team</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="testimonials.html">Testimonials</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="large-features.html">Large Features</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="pricing.html">Pricing</a></li>
                              </ul>
                            </li>
                          </ul> -->
                        </li>
                        <li class="rd-nav-item"><a class="rd-nav-link page-scroll" href="#servicios">SERVICIOS</a>
                        </li>
                        <li class="rd-nav-item"><a class="rd-nav-link page-scroll" href="#galeria">GALERIA</a>
                        <!-- <li class="rd-nav-item"><a class="rd-nav-link page-scroll" href="#clientes">CLIENTES</a> -->
                          <!-- RD Navbar Dropdown-->
                          <!-- <ul class="rd-menu rd-navbar-dropdown">
                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="grid-blog.html">Grid Blog</a></li>
                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="sidebar-blog.html">Sidebar Blog</a></li>
                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="single-blog-post.html">Single Blog Post</a></li>
                          </ul> -->
                        </li>
                        <li class="rd-nav-item"><a class="rd-nav-link page-scroll" href="#contacto">CONTACTO</a>
                        </li>
                        <li class="rd-nav-item"><a class="rd-nav-link page-scroll" href="#ubicacion">UBICACION</a>
                          <!-- RD Navbar Dropdown-->
                          <!-- <ul class="rd-menu rd-navbar-dropdown">
                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="grid-layout.html">Grid Layout</a></li>
                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="masonry-layout.html">Masonry Layout</a></li>
                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="modern-layout.html">Modern Layout</a></li>
                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="single-project.html">Single Project</a></li>
                          </ul> -->
                        </li>
                        <li class="rd-nav-item">
                          <button id="btn-success" class="btn btn-success btn-ingresar"><a class="rd-nav-link page-scroll ancla-ingresar" href="ingreso/">Ingresar</a></button>
                        </li>
                        <!-- <li class="rd-nav-item"><a class="rd-nav-link" href="#">Pages</a>
                          RD Navbar Megamenu
                          <ul class="rd-menu rd-navbar-megamenu">
                            <li class="rd-megamenu-item">
                              <div class="banner" style="background-image: url(images/megamenu-banner-1-570x368.jpg);"><a class='button button-sm button-primary button-winona' href='#get-template'>Buy Theme</a>
                              </div>
                            </li>
                            <li class="rd-megamenu-item">
                              <ul class="rd-megamenu-list">
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="404-page.html">404 Page</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="503-page.html">503 Page</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="coming-soon.html">Coming Soon</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="under-construction.html">Under Construction</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="about-us.html">About Us</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="about-me.html">About Me</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="grid-gallery.html">Grid Gallery</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="masonry-gallery.html">Masonry Gallery</a></li>
                              </ul>
                            </li>
                            <li class="rd-megamenu-item">
                              <ul class="rd-megamenu-list">
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="careers.html">Careers</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="search-results.html">Search results</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="services.html">Services</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="single-service.html">Single Service</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="single-job.html">Single Job</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="privacy-policy.html">Privacy policy</a></li>
                                <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="modern-gallery.html">Modern Gallery</a></li>
                              </ul>
                            </li>
                          </ul>
                        </li> -->
                        
                      </ul>
                    </div>
                    <!-- RD Navbar Search-->
                    <!-- <div class="rd-navbar-search" id="rd-navbar-search-1">
                      <button class="rd-navbar-search-toggle rd-navbar-fixed-element-2" data-rd-navbar-toggle="#rd-navbar-search-1"><span></span></button>
                      <form class="rd-search" action="search-results.html" data-search-live="rd-search-results-live-1" method="GET">
                        <div class="form-wrap">
                          <label class="form-label" for="rd-navbar-search-form-input-1">Search...</label>
                          <input class="form-input rd-navbar-search-form-input" id="rd-navbar-search-form-input-1" type="text" name="s" autocomplete="off">
                          <div class="rd-search-results-live" id="rd-search-results-live-1"></div>
                        </div>
                        <button class="rd-search-form-submit fa-search" type="submit"></button>
                      </form>
                    </div> -->

                  </div>
                </div>
              </div>
            </nav>
          </div>
        </header>
        <div class="parallax-container" data-parallax-img="images/parallax-1.jpg">
          <div class="parallax-content">
            <div class="jumbotron-classic-inner">
              <div class="container">
                <div  id="nosotros" class="jumbotron-classic-content row">
                  <div class="wow-outer col-lg-6 col-md-6">
                    <h6 class="title-decorated wow slideInLeft">SERVICIOS CARI</h6>
                  </div>

                  <div class="quienes-somos"> 
                    <h1 class="font-weight-bold wow-outer"><span class="wow slideInDown" data-wow-delay=".2s">Quienes Somos</span></h1>
                    <p class="big wow-outer"><span class="wow slideInDown" data-wow-delay=".35s">Somos una empresa con más de 12 años de experiencia, trabajando en hogares, empresas y oficinas brindando la mejor calidad de servicio, trabajamos con máquinas de alta calidad y potencia que brindan una limpieza profunda eliminando en un 99,9% los acaros, virus y bacterias.</span></p>
                    <div class="wow-outer button-outer"><a class="button button-lg button-primary button-winona wow slideInDown page-scroll" href="#servicios" data-wow-delay=".4s">Ver todos los servicios</a></div>
                  </div>
                  
                  <div class="wow-outer col-md-"></div>
                  <div class="wow-outer col-lg-4 offset-lg-1 col-md-12 contenedor-comentarios">
                      <ul class="list-sm offset-top-2 wow-outer" style="max-width: 380px;">
                          <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-whatsapp text-primary"></span><a class="link-default" href="tel:#"><h4 class=" wow slideInRight">+56 974865447</h4></a></li>
                      </ul>

                      <!-- inicio comentarios -->
                  <div class="col-md- col-md-push- animate-box" data-animate-effect="fadeInRight">
                    <div class="form-wrap form-wrap-comentarios">
                      <div class="tab">
                        
                        <div class="tab-content">
            
                          <div class="tab-content-inner active" data-content="signup">
                            <h3 class="h3-comentarios">CLIENTES SATISFECHOS</h3>
                              <marquee direction="up" scrolldelay="200">
                                <ul id="ul-comentarios" class="ul-comentarios">

                                  <?php 
                                    require_once ('models/modelComentario.php');
                                    $comentario=new modelComentario();									
                                    $comentario=$comentario->getComentario();
                                    echo $comentario;
                                  ?>
                                  
                                  
                                </ul>
                              </marquee>
                            <center class="center-comentarios" ><button class="button button-primary button-comentarios" data-toggle="modal" data-target="#ingreseComentario">Ingrese Comentario</button></center>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- fin comentarios -->
                  </div>
                  <!-- <div class="quienes-somos"> 
                      <h1 class="font-weight-bold wow-outer"><span class="wow slideInDown" data-wow-delay=".2s">Quienes Somos</span></h1>
                      <p class="big wow-outer"><span class="wow slideInDown" data-wow-delay=".35s">Somos una empresa con más de 12 años de experiencia, trabajando en hogares, empresas y oficinas brindando la mejor calidad de servicio, trabajamos con máquinas de alta calidad y potencia que brindan una limpieza profunda eliminando en un 99,9% los acaros, virus y bacterias.</span></p>
                      <div class="wow-outer button-outer"><a class="button button-lg button-primary button-winona wow slideInDown page-scroll" href="#servicios" data-wow-delay=".4s">Ver todos los servicios</a></div>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- inicio Modal -->
      <div class="modal fade" id="ingreseComentario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ingrese Comentario | Felicitación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                    </button>
                </div>
                  <div class="modal-body">
                    <span id="alert_cont"></span>
                    <form id="con_form">
                      <div class="form-group">
                        <label for="con_name">Nombre / Empresa</label>
                          <input type="text" class="form-control" id="nombre" name="con_name">
                      </div>
                      <div class="form-group">
                        <label for="con_tel">Teléfono</label>
                        <input type="tel" class="form-control" id="telefono" name="con_tel">
                      </div>
                      <div class="form-group">
                        <label for="con_email">Email</label>
                        <input type="email" class="form-control" id="email" name="con_email">
                      </div>
                      <div class="form-group">
                        <label for="con_mensaje">Comentario | Felicitación</label>
                        <textarea class="form-control" rows="5" id="comentario" name="con_mensaje"></textarea>
                      </div>
                    </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" id="submit_comentario">Enviar</button>
                  </div>
                </form>
              </div>
          </div>
      </div>
        <!-- fin modal -->

      <!-- Services-->
      <section id="servicios" class="section section-lg bg-gray-100 text-center">
        <div class="container"> 
          <h3 class="wow-outer"><span class="wow slideInUp">Nuestros Servicios</span></h3>
          <div class="row row-30">
            <div class="col-sm-6 col-lg-6 wow-outer">
              <!-- Box Creative-->
              <article class="box-creative wow slideInDown">
                <!-- <div class="box-creative-icon fl-bigmug-line-chat55"></div> -->
                <a class="box-creative-icon" href="#"><img src="images/lavado_piso.png" alt="Imagen servicio lavado de pisos" width="111" height="40" ></a>
                <h4 class="box-creative-title"><a href="#">Lavado de piso:</a></h4>
                <p class="text-left">Consiste en quitar la suciedad y grasas acumuladas en superficies, mediante uso de máquinas industriales como la abrillantadora con posterior secado.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-6 wow-outer">
              <!-- Box Creative-->
              <article class="box-creative wow slideInDown" data-wow-delay=".05s">
                <!-- <div class="box-creative-icon fl-bigmug-line-graphical8"></div> -->
                <a class="box-creative-icon" href="#"><img   src="images/lavado_alfombra.png" alt="Imagen servicio lavado de alfombras" width="111" height="40" ></a>
                <h4 class="box-creative-title"><a href="#">Lavado de alfombra:</a></h4>
                <p class="text-left">Consiste en el lavado con shampoo, enjuague, secado y sanitizacion. Eliminando bacterias de la misma.</p>
                
              </article>
            </div>
            <div class="col-sm-6 col-lg-6 wow-outer">
              <!-- Box Creative-->
              <article class="box-creative wow slideInDown" data-wow-delay=".1s">
                <!-- <div class="box-creative-icon fl-bigmug-line-circular220"></div> -->
                <a class="box-creative-icon" href="#"><img  src="images/lavado_tapiz.png" alt="Imagen servicio lavado de tapiz" width="111" height="40" ></a>
                <h4 class="box-creative-title"><a href="#">Lavado de tapiz:</a></h4>
                <p class="text-left">Es el lavado con máquina industrial que elimina manchas de agua, grasa, y todo tipo de suciedad profunda en sillones, sillas, asientos de automóviles, entre otros.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-6 wow-outer">
              <!-- Box Creative-->
              <article class="box-creative wow slideInDown" data-wow-delay=".1s">
                <!-- <div class="box-creative-icon fl-bigmug-line-wallet26"></div> -->
                <a class="box-creative-icon" href="#"><img   src="images/lavado_decapado.png" alt="Imagen servicio lavado decapado" width="111" height="40" ></a>
                <h4 class="box-creative-title"><a href="#">Decapado:</a></h4>
                <p class="text-left">Consiste en el lavado del piso mediante máquina abrillantadora y la utilización de pad suave, con la aplicación de líquidos de apartes para superficies de madera, piso flotante o cerámicos. Posteriormente se eliminan residuos con máquina Aspirador. El proceso permite eliminar suciedad y grasas Acumulada durante el tiempo.</p>
              </article>
            </div>
          </div>
        </div>
      </section>
      
      <!-- social boton inicio -->
      <div class="social">
        <ul>
          <li><a href="http://www.whatsapp.com/Servicioscari" target="_blank" class="mdi mdi-whatsapp"></a></li>
          <li><a href="http://www.instagram.com/Servicioscari" target="_blank" class="mdi mdi-instagram"></a></li>
          <li><a href="http://www.facebook.com/Servicioscari" target="_blank" class="mdi mdi-facebook"></a></li>
        </ul>
      </div>
      <!-- social boton fin -->

      <!-- redes sociales inicio -->
      <section id="galeria"class="section section-lg bg-primary-azul text-center">
        <div class="container">
          <div class="box-cta-1">
            <h3 style="color:white;" class="wow-outer"><span class="wow slideInRight">Redes Sociales <span class="font-weight-bold">Instagram</span></span></h3>
            
          </div>
          
           <!-- codepen inicio -->
      
          <div id="instafeed" class="gallery row no-gutter">
          
          </div>
          <button id="btn-instafeed-load" class="btn">Más</button>
          
          <!-- code pen fin -->
        
        </div>
      </section>
       <!-- redes sociales fin -->
      

      <!-- <section class="section-grande text-center">
        <div class="parallax-container" data-parallax-img="images/imagen_div.png">
          <div class="parallax-content">
            <div class="jumbotron-classic-inner">
              <div class="container">
                <div  id="nosotros" class="jumbotron-classic-content">
                  <div class="wow-outer">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <img  src="images/imagen_div_2.png"/>
      </section> -->

      
      <!-- Pricing-->
      <!-- <section class="section section-lg text-center">
        <div class="container">
          <h3 class="wow-outer"><span class="wow slideInUp">Pricing</span></h3>
          <p class="wow-outer"><span class="text-width-1 wow slideInDown">We understand the needs of modern companies, and that’s why we made our pricing policy affordable for every business owner. You can choose a suitable pricing plan below.</span></p>
          <div class="pricing-group-modern wow-outer">
            Pricing Modern
            <article class="pricing-modern wow fadeInLeft">
              <ul class="pricing-modern-rating">
                <li class="mdi mdi-star-outline"></li>
              </ul>
              <h5 class="pricing-modern-title"><a href="single-service.html">Small Business</a></h5>
              <table class="pricing-modern-table">
                <tr>
                  <td>2</td>
                  <td>Employees</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Month</td>
                </tr>
                <tr>
                  <td>1000</td>
                  <td>Minutes</td>
                </tr>
              </table>
              <p class="pricing-modern-price"><span class="pricing-modern-price-currency">$</span>789.00</p><a class="button button-primary button-winona" href="single-service.html">Order service</a>
            </article>
            Pricing Modern
            <article class="pricing-modern wow fadeInLeft" data-wow-delay=".05s">
              <ul class="pricing-modern-rating">
                <li class="mdi mdi-star-outline"></li>
                <li class="mdi mdi-star-outline"></li>
              </ul>
              <h5 class="pricing-modern-title"><a href="single-service.html">Medium Business</a></h5>
              <table class="pricing-modern-table">
                <tr>
                  <td>5</td>
                  <td>Employees</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Months</td>
                </tr>
                <tr>
                  <td>2000</td>
                  <td>Minutes</td>
                </tr>
              </table>
              <p class="pricing-modern-price"><span class="pricing-modern-price-currency">$</span>1299.00</p><a class="button button-primary button-winona" href="single-service.html">Order service</a>
            </article>
            Pricing Modern
            <article class="pricing-modern wow fadeInLeft" data-wow-delay=".1s">
              <ul class="pricing-modern-rating">
                <li class="mdi mdi-star-outline"></li>
                <li class="mdi mdi-star-outline"></li>
                <li class="mdi mdi-star-outline"></li>
              </ul>
              <h5 class="pricing-modern-title"><a href="single-service.html">Big Business</a></h5>
              <table class="pricing-modern-table">
                <tr>
                  <td>10</td>
                  <td>Employees</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Months</td>
                </tr>
                <tr>
                  <td>4000</td>
                  <td>Minutes</td>
                </tr>
              </table>
              <p class="pricing-modern-price"><span class="pricing-modern-price-currency">$</span>2369.00</p><a class="button button-primary button-winona" href="single-service.html">Order service</a>
            </article>
            Pricing Modern
            <article class="pricing-modern wow fadeInLeft" data-wow-delay=".15s">
              <ul class="pricing-modern-rating">
                <li class="mdi mdi-star-outline"></li>
                <li class="mdi mdi-star-outline"></li>
                <li class="mdi mdi-star-outline"></li>
                <li class="mdi mdi-star-outline"></li>
              </ul>
              <h5 class="pricing-modern-title"><a href="single-service.html">Corporation</a></h5>
              <table class="pricing-modern-table">
                <tr>
                  <td>20+</td>
                  <td>Employees</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Months</td>
                </tr>
                <tr>
                  <td>8000+</td>
                  <td>Minutes</td>
                </tr>
              </table>
              <p class="pricing-modern-price"><span class="pricing-modern-price-currency">$</span>6790.00</p><a class="button button-primary button-winona" href="single-service.html">Order service</a>
            </article>
          </div>
        </div>
      </section> -->


      <!-- Testimonials & Video-->
      <!-- <section class="section section-lg bg-gray-100">
        <div class="container">
          <h3 class="wow-outer text-center"><span class="wow slideInUp">Testimonios</span></h3>
          <div class="row row-50 justify-content-center justify-content-lg-between">
            <div class="col-md-10 col-lg-5 wow-outer">
              <div class="owl-carousel wow slideInLeft" data-autoplay="true" data-items="1" data-dots="true" data-nav="false" data-loop="true" data-margin="30" data-stage-padding="0" data-mouse-drag="false">
                <blockquote class="quote-modern">
                  <svg class="quote-modern-mark" x="0px" y="0px" width="35px" height="25px" viewbox="0 0 35 25">
                    <path d="M27.461,10.206h7.5v15h-15v-15L25,0.127h7.5L27.461,10.206z M7.539,10.206h7.5v15h-15v-15L4.961,0.127h7.5                L7.539,10.206z"></path>
                  </svg>
                  <div class="quote-modern-text">
                    <p>Nos definimos como un centro de contacto de la siguiente generación. Nuestras decisiones estratégicas han implicado la ampliación del grupo hacia el mundo de la tecnología. Hemos creado una Empresa ligada que desarrolla para Latinoamérica software para centros de contacto. Lo anterior ha permitido que todos nuestros clientes, no solo disfruten de nuestros resultados, sino además de todas las herramientas tecnológica que poseemos. Dentro de las cuales se encuentran: Árboles decisionales cognitivos, Speech Analytics, Bots  de conversación, entre otros.</p>
                  </div>
                  <div class="quote-modern-meta">
                    <div class="quote-modern-avatar box-minimal-icon fl-bigmug-line-user144"><img src="images/ceo.jpeg" alt="" width="96" height="96"/>
                    </div>
                    <div class="quote-modern-info">
                      <cite class="quote-modern-cite">Jorge Leiva</cite>
                      <p class="quote-modern-caption">CEO</p>
                    </div>
                  </div>
               
              </div>
            </div>
            <div class="col-md-10 col-lg-6 wow-outer">
              <div class="thumbnail-video-1 bg-gray-700 wow slideInLeft">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe width="570" height="320" src="//www.youtube.com/embed/UtGVMBRgqwo?rel=0" allowfullscreen=""></iframe>
                </div>
                <div class="thumbnail-video__overlay video-overlay" style="background-image: url(images/video-preview-1-570x320.jpg)">
                  <div class="button-video"></div>
                  <h5>SERVICIOS CARI</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> -->

      <!-- CTA Thin-->
      <!-- <section class="section section-xs bg-primary-darker text-center">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-sm-10 col-md-12">
              <div class="box-cta-thin">
                <h4 class="wow-outer"><span class="wow slideInRight">Learn the Cost of Personalized <span class="font-weight-bold">Customer Support Service</span> Now!</span></h4>
                <div class="wow-outer button-outer"><a class="button button-primary button-winona wow slideInLeft" href="pricing.html">View all pricing</a></div>
              </div>
            </div>
          </div>
        </div>
      </section> -->

      <!-- Our Team-->
      <!-- <section class="section section-lg text-center">
        <div class="container">
          <h3 class="wow-outer"><span class="wow slideInUp">Our Team</span></h3>
          <div class="row row-50">
            <div class="col-sm-6 col-lg-3 wow-outer">
              Profile Classic
              <article class="profile-classic wow slideInLeft">
                <div class="profile-classic-main"><img class="profile-classic-image" src="images/team-1-270x273.jpg" alt="" width="270" height="273"/>
                  <div class="profile-classic-caption">
                    <div class="group group-xs group-middle"><a class="icon icon-sm icon-creative mdi mdi-facebook" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-twitter" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-instagram" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-google" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-linkedin" href="#"></a></div>
                  </div>
                </div>
                <h5 class="profile-classic-title"><a href="#">Nathan Porter</a></h5>
                <p class="profile-classic-position">CEO, Founder</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-3 wow-outer">
              Profile Classic
              <article class="profile-classic wow slideInLeft" data-wow-delay=".05s">
                <div class="profile-classic-main"><img class="profile-classic-image" src="images/team-2-270x273.jpg" alt="" width="270" height="273"/>
                  <div class="profile-classic-caption">
                    <div class="group group-xs group-middle profile-classic-group"><a class="icon icon-sm icon-creative mdi mdi-facebook" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-twitter" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-instagram" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-google" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-linkedin" href="#"></a></div>
                  </div>
                </div>
                <h5 class="profile-classic-title"><a href="#">Jean Thompson</a></h5>
                <p class="profile-classic-position">Project Manager</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-3 wow-outer">
              Profile Classic
              <article class="profile-classic wow slideInLeft" data-wow-delay=".1s">
                <div class="profile-classic-main"><img class="profile-classic-image" src="images/team-3-270x273.jpg" alt="" width="270" height="273"/>
                  <div class="profile-classic-caption">
                    <div class="group group-xs group-middle profile-classic-group"><a class="icon icon-sm icon-creative mdi mdi-facebook" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-twitter" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-instagram" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-google" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-linkedin" href="#"></a></div>
                  </div>
                </div>
                <h5 class="profile-classic-title"><a href="#">Brian Payne</a></h5>
                <p class="profile-classic-position">Manager</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-3 wow-outer">
              Profile Classic
              <article class="profile-classic wow slideInLeft" data-wow-delay=".15s">
                <div class="profile-classic-main"><img class="profile-classic-image" src="images/team-4-270x273.jpg" alt="" width="270" height="273"/>
                  <div class="profile-classic-caption">
                    <div class="group group-xs group-middle profile-classic-group"><a class="icon icon-sm icon-creative mdi mdi-facebook" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-twitter" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-instagram" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-google" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-linkedin" href="#"></a></div>
                  </div>
                </div>
                <h5 class="profile-classic-title"><a href="#">Marie Fernandez</a></h5>
                <p class="profile-classic-position">Marketing Manager</p>
              </article>
            </div>
          </div>
          <div class="wow-outer button-outer"><a class="button button-lg button-primary button-winona wow slideInUp" href="#">View all Team</a></div>
        </div>
      </section> -->

      <!-- Careers-->
      <!-- <section class="section section-lg bg-gray-100 text-center">
        <div class="container">
          <h3 class="wow-outer"><span class="wow slideInDown">Careers</span></h3>
          <div class="row row-50">
            <div class="col-md-6 col-lg-4 wow-outer">
                    Career Classic
                    <article class="career-classic wow slideInRight">
                      <h4 class="career-classic-title"><a href="single-job.html">Customer Assistant</a></h4>
                      <div class="career-classic-divider"></div>
                      <ul class="career-classic-list">
                        <li><span class="icon mdi mdi-calendar-clock"></span><span>Full Time</span></li>
                        <li><span class="icon mdi mdi-map-marker"></span><span>San Diego</span></li>
                      </ul>
                      <p>Want to build your future with the biggest customer service provider in the US? Then apply for this job and become one of the CallCenter customer care voices.</p>
                    </article>
            </div>
            <div class="col-md-6 col-lg-4 wow-outer">
                    Career Classic
                    <article class="career-classic wow slideInRight" data-wow-delay=".05s">
                      <h4 class="career-classic-title"><a href="single-job.html">Customer Care Manager</a></h4>
                      <div class="career-classic-divider"></div>
                      <ul class="career-classic-list">
                        <li><span class="icon mdi mdi-calendar-clock"></span><span>Full Time</span></li>
                        <li><span class="icon mdi mdi-map-marker"></span><span>San Diego</span></li>
                      </ul>
                      <p>CallCenter is looking for outstanding managers to work with Customer Care Agents whose role will be to answer questions and resolve customer issues.</p>
                    </article>
            </div>
            <div class="col-md-6 col-lg-4 wow-outer">
                    Career Classic
                    <article class="career-classic wow slideInRight" data-wow-delay=".1s">
                      <h4 class="career-classic-title"><a href="single-job.html">Technical Support Agent</a></h4>
                      <div class="career-classic-divider"></div>
                      <ul class="career-classic-list">
                        <li><span class="icon mdi mdi-calendar-clock"></span><span>Full Time</span></li>
                        <li><span class="icon mdi mdi-map-marker"></span><span>San Diego</span></li>
                      </ul>
                      <p>We are looking for full-time Technical Support Representatives in San Diego, CA. Our team needs specialists who areready to improve the quality of customer care.</p>
                    </article>
            </div>
          </div>
          <div class="wow-outer button-outer"><a class="button button-lg button-primary button-winona wow slideInUp" href="careers.html">Join Us</a></div>
        </div>
      </section> -->

      <!-- Accordion & photo-->
      <!-- <section class="section section-md">
        <div class="container">
          <div class="row row-50 justify-content-lg-between flex-lg-row-reverse offset-top-1">
            <div class="col-lg-7 col-xl-6">
              <h3 class="wow-outer"><span class="wow slideInDown">Frequently Asked Questions</span></h3>
              Bootstrap collapse
              <div class="card-group-custom card-group-corporate wow-outer" id="accordion2" role="tablist" aria-multiselectable="false">
                          Bootstrap card
                          <article class="card card-custom card-corporate wow fadeInDown" data-wow-delay=".05s">
                            <div class="card-header" id="accordion2-heading-1" role="tab">
                              <div class="card-title"><a role="button" data-toggle="collapse" data-parent="#accordion2" href="#accordion2-collapse-1" aria-controls="accordion2-collapse-1" aria-expanded="true">What are the advantages of purchasing a website template?
                                  <div class="card-arrow"></div></a></div>
                            </div>
                            <div class="collapse show" id="accordion2-collapse-1" role="tabpanel" aria-labelledby="accordion2-heading-1">
                              <div class="card-body">
                                <p>The major advantage is price: You get a high quality design for just $20-$70. You don’t have to hire a web designer or web design studio. Second advantage is time frame: It usually takes 5-15 days for a good designer to produce a web page of such quality.</p>
                              </div>
                            </div>
                          </article>
                          Bootstrap
                          <article class="card card-custom card-corporate wow fadeInDown" data-wow-delay=".1s">
                            <div class="card-header" id="accordion2-heading-2" role="tab">
                              <div class="card-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#accordion2-collapse-2" aria-controls="accordion2-collapse-2" aria-expanded="false">Do you provide any scripts with your templates or could you do some custom programming?
                                  <div class="card-arrow"></div></a></div>
                            </div>
                            <div class="collapse" id="accordion2-collapse-2" role="tabpanel" aria-labelledby="accordion2-heading-2">
                              <div class="card-body">
                                <p>Our templates do not include any additional scripts. Newsletter subscriptions, search fields, forums, image galleries (in HTML versions of Flash products) are inactive. Basic scripts can be easily added at www.zemez.io If you are not sure that the element you’re interested in is active please contact our Support Chat for clarification.</p>
                              </div>
                            </div>
                          </article>
                          Bootstrap card
                          <article class="card card-custom card-corporate wow fadeInDown" data-wow-delay=".15s">
                            <div class="card-header" id="accordion2-heading-3" role="tab">
                              <div class="card-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#accordion2-collapse-3" aria-controls="accordion2-collapse-3" aria-expanded="false">What do I receive when I order a template from Zemez?
                                  <div class="card-arrow"></div></a></div>
                            </div>
                            <div class="collapse" id="accordion2-collapse-3" role="tabpanel" aria-labelledby="accordion2-heading-3">
                              <div class="card-body">
                                <p>After you complete the payment via our secure form you will receive the instructions for downloading the product. The source files in the download package can vary based on the type of the product you have purchased.</p>
                              </div>
                            </div>
                          </article>
                          Bootstrap card
                          <article class="card card-custom card-corporate wow fadeInDown" data-wow-delay=".2s">
                            <div class="card-header" id="accordion2-heading-4" role="tab">
                              <div class="card-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#accordion2-collapse-4" aria-controls="accordion2-collapse-4" aria-expanded="false">In what formats are your templates available?
                                  <div class="card-arrow"></div></a></div>
                            </div>
                            <div class="collapse" id="accordion2-collapse-4" role="tabpanel" aria-labelledby="accordion2-heading-4">
                              <div class="card-body">
                                <p>Website templates are available in Photoshop and HTML formats. Fonts are included with the Photoshop file. In most templates, HTML is compatible with Adobe® Dreamweaver® and Microsoft® FrontPage®.</p>
                              </div>
                            </div>
                          </article>
              </div>
            </div>
            <div class="col-lg-5 wow-outer"><img class="wow slideInLeft" src="images/accordions-1-470x368.jpg" alt="" width="470" height="368"/>
            </div>
          </div>
        </div>
      </section>
       -->
      <!-- Wide CTA-->
      <!-- <section id="clientes"class="section section-md bg-primary-darker text-center">
        <div class="container">
          <div class="box-cta-1">
            <h3 class="wow-outer"><span class="wow slideInRight">Clientes <span class="font-weight-bold">Servicios Cari</span></span></h3>
            <div class="wow-outer button-outer"><a class="button button-lg button-primary button-winona wow slideInLeft" href="#">Free consultation</a></div>
          </div>
        </div>
      </section> -->
      <!-- Partners-->
      <!-- <section class="section section-sm text-center">
        <div class="container">
          Owl Carousel
          <div class="owl-carousel owl-carousel-1" data-autoplay="true" data-items="2" data-sm-items="3" data-md-items="4" data-lg-items="5" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
            <div class="item wow-outer">
              <div class="wow slideInLeft"><a class="link-image" href="#"><img src="images/cliente-1.jpeg" alt="" width="135" height="87"/></a></div>
            </div>
            <div class="item wow-outer">
              <div class="wow slideInLeft" data-wow-delay=".05s"><a class="link-image" href="#"><img src="images/cliente-2.jpeg" alt="" width="134" height="104"/></a></div>
            </div>
            <div class="item wow-outer">
              <div class="wow slideInLeft" data-wow-delay=".1s"><a class="link-image" href="#"><img src="images/cliente-3.jpeg" alt="" width="132" height="87"/></a></div>
            </div>
            <div class="item wow-outer">
              <div class="wow slideInLeft" data-wow-delay=".15s"><a class="link-image" href="#"><img src="images/cliente-4.jpeg" alt="" width="143" height="85"/></a></div>
            </div>
            <div class="item wow-outer">
              <div class="wow slideInLeft" data-wow-delay=".2s"><a class="link-image" href="#"><img src="images/cliente-5.jpeg" alt="" width="111" height="85"/></a></div>
            </div>
            <div class="item wow-outer">
                <div class="wow slideInLeft" data-wow-delay=".25s"><a class="link-image" href="#"><img src="images/cliente-6.jpeg" alt="" width="111" height="85"/></a></div>
            </div>
            <div class="item wow-outer">
                <div class="wow slideInLeft" data-wow-delay=".3s"><a class="link-image" href="#"><img src="images/cliente-7.jpeg" alt="" width="111" height="85"/></a></div>
            </div>
          </div>
        </div>
      </section> -->

       <!-- barra  formulario CTA Thin-->
      <section class="section section-xs bg-primary-darker text-center">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-sm-10 col-md-12">
              <div class="box-cta-thin">
                <h4 class="wow-outer"><span class="wow slideInRight">Contáctenos <span class="font-weight-bold">estamos atentos a sus consultas</h4>
                <!-- <div class="wow-outer button-outer"><a class="button button-primary button-winona wow slideInLeft" href="pricing.html">View all pricing</a></div> -->
              </div>
            </div>
          </div>
        </div>
      </section>

       <!--  contact form-->
       <!-- <div id="contacto" class="section-sm"> -->
       <div id="contacto">
        <div class="container">
          <!-- <h6> contact form </h6> -->
        </div>
        <section class="section bg-gray-100" style="margin-top: 0px;">
          <div class="range justify-content-xl-between">
              <div class="cell-xl-6 align-self-center height-fill wow fadeIn">
                  <div class="cell-xl-6 align-self-center wow-outer"><img class="wow slideInLeft" src="images/accordions-1-470x368.jpg" alt="Imagen de contacto" width="470" height="368"/>
                </div>
              </div>
            <div class="cell-xl-6 align-self-center container">
              <div class="row">
                <div class="col-lg-9 cell-inner">
                  <div class="section-lg">
                    <h3 class="wow-outer"><span class="wow slideInDown" style="color:black;">Contactar</span></h3>
                    <!-- RD Mailform-->
                    <!-- <form id="form_contacto" class="rd-form rd-mailform" data-form-output="form-output-global" data-form-type="contact" method="post"></form> -->
                    <span id="respuesta"></span>
                    <form id="form_contacto" class="rd-form rd-mailform" data-form-output="" data-form-type="contact" method="post">
                      <div class="row row-10">
                        <div class="col-md-6 wow-outer">
                          <div class="form-wrap wow fadeSlideInUp">
                            <label class="form-label-outside" for="contact-first-name">Nombre</label>
                            <input class="form-input" id="contact-first-name" type="text" name="name" data-constraints="@Required">
                          </div>
                        </div>
                        <div class="col-md-6 wow-outer">
                          <div class="form-wrap wow fadeSlideInUp">
                            <label class="form-label-outside" for="contact-last-name">Apellido</label>
                            <input class="form-input" id="contact-last-name" type="text" name="apellido" data-constraints="@Required">
                          </div>
                        </div>
                        <div class="col-md-6 wow-outer">
                          <div class="form-wrap wow fadeSlideInUp">
                            <label class="form-label-outside" for="contact-email">E-mail</label>
                            <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                          </div>
                        </div>
                        <div class="col-md-6 wow-outer">
                          <div class="form-wrap wow fadeSlideInUp">
                            <label class="form-label-outside" for="contact-phone">Teléfono</label>
                            <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@PhoneNumber">
                          </div>
                        </div>
                        <div class="col-12 wow-outer">
                          <div class="form-wrap wow fadeSlideInUp">
                            <label class="form-label-outside" for="contact-message">Tu Mensaje</label>
                            <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="group group-middle">
                        <div class="wow-outer div_btn_enviar">
                          <button id="btn_enviar" class="button button-primary button-winona wow slideInRight" type="submit">Enviar Mensaje</button>
                        </div>
                        <!-- <p>or use</p>
                        <div class="wow-outer"><a class="button button-primary-outline button-icon button-icon-left button-winona wow slideInLeft" href="#"><span class="icon text-primary mdi mdi-facebook-messenger"></span>Messenger</a></div> -->
                      </div>
                    </form>
                    <!-- <ul class="list-sm offset-top-2 wow-outer" style="max-width: 380px;">
                  
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-map-marker text-primary"></span><a class="link-default" href="#">JR. Camana 390 Piso 2 <br> Cercado de Lima-Peru </a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-map-marker text-primary"></span><a class="link-default" href="#">Huerfanos 757 Oficina 210, <br> Santiago-Chile</a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-map-marker text-primary"></span><a class="link-default" href="#">Barros Borgoño Oficina 305, <br> Providencia Santiago-Chile</a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi fl-bigmug-line-note35 text-primary"></span><a class="link-default" href="mailto:#">Jorge Leiva Quiroz | Gerente General</a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-email text-primary"></span><a class="link-default" href="mailto:#">jorge@co-latam.com </a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-phone text-primary"></span>
                        <ul class="list-0">
                          <li><a class="link-default" href="tel:#">+56 942-442-305</a></li>
                          <li><a class="link-default" href="tel:#">+51 994-245-950</a></li>
                        </ul>
                      </li>
                    </ul> -->
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </section>
      </div>

      <!-- barra naranja unicacion CTA Thin-->
      <section class="section section-xs bg-primary-darker text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-sm-10 col-md-12">
                <div class="box-cta-thin">
                  <h4 class="wow-outer"><span class="wow slideInRight">Ubicación</h4>
                  <!-- <div class="wow-outer button-outer"><a class="button button-primary button-winona wow slideInLeft" href="pricing.html">View all pricing</a></div> -->
                </div>
              </div>
            </div>
          </div>
        </section>
      <!-- Map & contact information-->
      <section id="ubicacion" class="section bg-gray-100">
          <div class="range justify-content-sm-between flex-sm-row-reverse">
            <div class="cell-sm-6 cell-lg-5 cell-xl-4 align-self-center container">
              <div class="cell-inner">
                <div class="section-lg inset-left-1">
                  <h3 class="wow-outer"><span class="wow slideInUp">Contacto</span></h3>
                  <ul class="list-sm offset-top-2 wow-outer" style="max-width: 380px;">
                      <!-- <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-map-marker text-primary"></span><a class="link-default" href="#">JR. Camana 390 Piso 2 <br> Cercado de Lima-Peru </a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-map-marker text-primary"></span><a class="link-default" href="#">Huerfanos 757 Oficina 210, <br> Santiago-Chile</a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-map-marker text-primary"></span><a class="link-default" href="#">Barros Borgoño Oficina 305, <br> Providencia Santiago-Chile</a></li> -->
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi fl-bigmug-line-note35 text-primary"></span><a class="link-default" href="mailto:#">SERVICIOS CARI</a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-email text-primary"></span><a class="link-default" href="mailto:#">contacto@servicioscari.cl </a></li>
                      <li class="object-inline wow slideInLeft"><span class="icon icon-md mdi mdi-phone text-primary"></span>
                        <ul class="list-0 telefonos">

                          <!-- <li><a class="link-default" href="tel:#">+56 974865447</a></li>
                          <li><a class="link-default" href="tel:#">+56 977376559</a></li> -->
                        </ul>
                      </li>
                    </ul>
                  <div class="group group-xs group-middle"><a class="icon icon-sm icon-creative mdi mdi-facebook" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-twitter" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-instagram" href="http://www.instagram.com/Servicioscari" target="_blank"></a><a class="icon icon-sm icon-creative mdi mdi-google" href="#"></a></div>
                </div>
              </div>
            </div>
            <div class="cell-sm-6 cell-lg-6 cell-xl-7 height-fill">
                <div class="rd-google-map rd-google-map__model" data-marker="images/gmap_marker.svg" data-marker-active="images/gmap_marker_active.svg" data-styles="[{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#e9e9e9&quot;},{&quot;lightness&quot;:17}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f5f5f5&quot;},{&quot;lightness&quot;:20}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;},{&quot;lightness&quot;:17}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;},{&quot;lightness&quot;:29},{&quot;weight&quot;:0.2}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;},{&quot;lightness&quot;:18}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;},{&quot;lightness&quot;:16}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f5f5f5&quot;},{&quot;lightness&quot;:21}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#dedede&quot;},{&quot;lightness&quot;:21}]},{&quot;elementType&quot;:&quot;labels.text.stroke&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;color&quot;:&quot;#ffffff&quot;},{&quot;lightness&quot;:16}]},{&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:36},{&quot;color&quot;:&quot;#333333&quot;},{&quot;lightness&quot;:40}]},{&quot;elementType&quot;:&quot;labels.icon&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;},{&quot;lightness&quot;:19}]},{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#fefefe&quot;},{&quot;lightness&quot;:20}]},{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#fefefe&quot;},{&quot;lightness&quot;:17},{&quot;weight&quot;:1.2}]}]" data-zoom="14" data-x="-70.6413263,17" data-y="-33.3699083">
                  <ul class="map_locations">
                    <li data-y="-33.3699083" data-x="-70.6413263,17">
                      <p>General Las Heras #934 Huechuraba.</p>
                    </li>
                  </ul>
                </div>
            </div>
          </div>
        </section>


      <!-- Page Footer-->
      <footer class="section footer-minimal bg-gray-700">
        <div class="container"> 
          <div class="footer-minimal-inner"><a class="brand" href="index.html"><img src="images/lavado_tapiz.png" alt="Logo servicioscari" width="111" height="40" srcset="images/lavado_tapiz.png 2x"/><p class="parrafo-logo">SERVICIOS CARI</p> </p></p></a>
            <!-- Rights-->
            <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>Todos los derechos reservados.</span><span>&nbsp;</span><br class="d-sm-none"/></p>
          </div>
        </div>
      </footer>
    </div>
    <div class="preloader"> 
      <div class="preloader-logo"><img src="images/lavado_tapiz.png" alt="Preloader logo servicioscari" width="111" height="45" srcset="images/lavado_tapiz.png 2x"/>
      </div>
      <div class="preloader-body">
        <div class="transition-loader">
          <div class="transition-loader-inner">
            <label></label>
            <label></label>
            <label></label>
            <label></label>
            <label></label>
            <label></label>
          </div>
        </div>
      </div>
    </div>
    <!-- PANEL-->
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- cuando se envie el form agregar la clase active y si todo sale bien, quitarla -->
    <!-- Javascript-->
    <script src="js/jquery.js"></script>
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/page-scroll.js"></script>
    <script src="js/sweetalert2.js"></script>

    <!-- jquery validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
    
    <!-- Llamada para mostrar tel, ws, email activos -->
	  <script src="js/controllers/controllerTelefono/controllerTelefonos.js" ></script>
	
	  <!-- enviar comentario js -->
	  <script src="js/controllers/controllerComentario/controllerInsertComentario.js" ></script>
    
    <script src="js/controllers/controllerContacto.js"></script>
    
    <!-- Magnific Popup -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- para consumir API de instagram -->
	  <script type="text/javascript" src="js/instafeed.min.js"></script>
    <!-- para Mostrar fotos con la API de instagram -->
    <script type="text/javascript" src="js/fotos_instagram.js"></script>
    <!-- Chat Drift -->
    <script async src="js/drift.js"></script>
    
  </body>
</html>
