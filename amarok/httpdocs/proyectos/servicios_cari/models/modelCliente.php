<?php

require_once ('db.php');

class modelCliente {
private $db;
private $session;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->session = session_start();
    }
    
    //metodo para usar en edit_perfil
    public function getDB(){
        return $this->db;
    }
    // metodos para los usuarios
    //inicio Modal  /insertar/editar/borrar usuario
    public function ModalEditUsuarios() {
        $query = '';
        $query_editar = '';
        $output = array();
        //si viene vacio es porque agrega un nuevo usuario
        if($_POST['btn_action'] == '')
        {
            $query_agregar = "
            INSERT INTO cliente (nombre, apellido, telefono, email, comentario, rut, cliente, direccion, comuna) 
            VALUES ('".$_POST["user_name"]."', '".$_POST["user_apellido"]."', '".$_POST["user_telefono"]."',
                    '".$_POST["user_email"]."', '".$_POST["user_comentario"]."', '".$_POST["user_rut"]."',
                    '".$_POST["user_cliente"]."', '".$_POST["user_direccion"]."', '".$_POST["user_comuna"]."' );
            "; 
            $consultaAgregar = $this->db->query($query_agregar);
            
            if(isset($consultaAgregar))
            {
            echo 'Nuevo Usuario Agregado '.$consultaAgregar;
            }
        }

        if($_POST['btn_action'] == 'fetch_single')
        {
            
            $query = "SELECT * FROM cliente WHERE id ='".$_POST['user_id']."'";
            
            //ejecutamos la consulta
            $consulta = $this->db->query($query);
            $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
            
            foreach($respuesta as $row)
            {
                $output['user_name'] = $row['nombre'];
                $output['user_apellido'] = $row['apellido'];
                $output['user_telefono'] = $row['telefono'];
                $output['user_email'] = $row['email'];
                $output['user_comentario'] = $row['comentario'];
                $output['user_rut'] = $row['rut'];
                $output['user_cliente'] = $row['cliente'];
                $output['user_direccion'] = $row['direccion'];
                $output['user_comuna'] = $row['comuna'];
            }
            
            echo json_encode($output);   
            
        }
        
        if($_POST['btn_action'] == 'Edit')
        {
            $query_editar = "UPDATE cliente SET nombre = '".$_POST["user_name"]."',
                apellido = '".$_POST["user_apellido"]."',
                telefono = '".$_POST["user_telefono"]."',
                email = '".$_POST["user_email"]."',
                comentario = '".$_POST["user_comentario"]."',
                rut = '".$_POST["user_rut"]."',
                cliente = '".$_POST["user_cliente"]."',
                direccion = '".$_POST["user_direccion"]."',
                comuna = '".$_POST["user_comuna"]."'
                WHERE id = '".$_POST["user_id"]."' ";
                
            $consultaEditar = $this->db->query($query_editar);
            // $respuestaEditar = $consultaEditar->fetch_all(MYSQLI_ASSOC);
            if(isset($consultaEditar))
            {
                echo 'Usuario Editado '.$consultaEditar;
            }
        }
        //cambiar estatus
        if($_POST['btn_action'] == 'cambiar'){
        
            $status = 'Si';
            if($_POST['status'] == 'Si'){
                
                $status = 'No';
            }
            $query_estatus = "
                update cliente
                set cliente = '".$status."'
                where id = '".$_POST["user_id"]."'
            ";
                
            $result = $this->db->query($query_estatus);
            if(isset($result)){
                
                echo 'Estatus Cambiado a ' . $status;
            }
        }

        // condicion para eliminar la fila seleccionada
        if($_POST['btn_action'] == 'borrar'){
        
            $query_delete = "
                delete  from cliente
                where id = '".$_POST["user_id"]."'
            ";
                
            $result = $this->db->query($query_delete);
            if(isset($result)){
                
                echo 'Datos Eliminados '.$result;
            }
        }

    }
    //fin Modal  /insertar/editar/borrar usuario

    // metodo para obtener todos los Usuarios
    public function getAllUsuarios() {

        $query = '';

        $output = array();

        $query .= " SELECT * FROM cliente ";
        if(isset($_POST["search"]["value"]))
        {
            $query .= 'where email LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR nombre LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR apellido LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR telefono LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR email LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR rut LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR cliente LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR direccion LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR comuna LIKE "%'.$_POST["search"]["value"].'%" ';
        }
        if(isset($_POST["order"]))
        {
            $query .= 'ORDER by '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
        }
        else{
            $query .= 'ORDER BY id DESC ';
        }
        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT ' .$_POST['start'] . ', ' . $_POST['length'];
        }


        //ejecutamos la consulta
        $consulta = $this->db->query($query);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        $data = array();
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $filtered_rows = count($respuesta);
        foreach($respuesta as $row)
        {
            $status = '';
            if($row['cliente'] == 'Si')
            {
                $status = '<span class="label label-success">Si</span>';
            }
            else
            {
                $status = '<span class="label label-danger">No</span>';
            }
            $sub_array = array();
            $sub_array[] = $row['id'];
            $sub_array[] = $row['nombre'];
            $sub_array[] = $row['apellido'];
            $sub_array[] = $row['telefono'];
            $sub_array[] = $row['email'];
            $sub_array[] = $row['rut'];
            $sub_array[] = $row['comentario'];
            $sub_array[] = $status;
            $sub_array[] = $row['direccion'];
            $sub_array[] = $row['comuna'];
            $sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-xs update">Actualizar</button>';
            $sub_array[] = '<button type="button" name="cambiar" id="'.$row["id"].'" class="btn btn-danger btn-xs cambiar" data-status="'.$row["cliente"].'">Cambiar Estatus</button>';
            $sub_array[] = '<button type="button" name="borrar" id="'.$row["id"].'" class="btn btn-danger btn-xs borrar">Eliminar Usuario</button>';
            $data[]      = $sub_array;
        }
        
        $tabla = 'cliente';
        $output = array(
            "draw"              => intval($_POST["draw"]),
            "recordsTotal"      => $filtered_rows,
            "recordsFiltered"   => self::get_total_comentarios($tabla),
            "data"              => $data
        );

        return json_encode($output);
    }
    // fin metodo para los usuarios


    public function get_total_comentarios($tabla)
    {
        $comentarios = '';
        $comentarios = " SELECT * FROM $tabla";
        //ejecutamos la consulta
        $consulta = $this->db->query($comentarios);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $totalRows = count($respuesta);
        return $totalRows;
    }

}
?>