<?php

require_once ('db.php');

class modelLogin {
private $db;
private $session;

    public function __construct() {
        $this->db = Conectar::conexion();
        $this->session = session_start();
    }
    
    //metodo para usar en edit_perfil
    public function getDB(){
        return $this->db;
    }
    // metodos para los usuarios
    //inicio Modal  /insertar/editar/borrar usuario
    public function ModalEditUsuarios() {
        $query = '';
        $query_editar = '';
        $output = array();
        //si viene vacio es porque agrega un nuevo usuario
        if($_POST['btn_action'] == '')
        {
            $query_agregar = "
            INSERT INTO usuario (correo_name, user_password, user_name, user_type, estatus) 
            VALUES ('".$_POST["user_email"]."', '".password_hash($_POST["user_new_password2"], PASSWORD_DEFAULT)."',
                    '".$_POST["user_name"]."', '".$_POST["user_type"]."', '".$_POST["estatus"]."' );
            "; 
            $consultaAgregar = $this->db->query($query_agregar);
            
            if(isset($consultaAgregar))
            {
            echo 'Nuevo Usuario Agregado';
            }
        }

        if($_POST['btn_action'] == 'fetch_single')
        {
            
            $query = "SELECT * FROM usuario WHERE id ='".$_POST['user_id']."'";
            
            //ejecutamos la consulta
            $consulta = $this->db->query($query);
            $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
            
            foreach($respuesta as $row)
            {
                $output['user_email'] = $row['correo_name'];
                $output['user_name'] = $row['user_name'];
                $output['user_type'] = $row['user_type'];
                $output['user_estatus'] = $row['estatus'];
            }
            
            echo json_encode($output);   
            
        }
        
        if($_POST['btn_action'] == 'Edit')
        {
            if($_POST['user_new_password2'] != ''){
                $query_editar = "UPDATE usuario SET correo_name = '".$_POST["user_email"]."',
                    user_password = '".password_hash($_POST["user_new_password2"], PASSWORD_DEFAULT)."',
                    user_name = '".$_POST["user_name"]."',
                    user_type = '".$_POST["user_type"]."',
                    estatus = '".$_POST["estatus"]."'
                    WHERE id = '".$_POST["user_id"]."' ";
            }
            else{
                $query_editar = "UPDATE usuario SET correo_name = '".$_POST["user_email"]."',
                    user_name = '".$_POST["user_name"]."',
                    user_type = '".$_POST["user_type"]."',
                    estatus = '".$_POST["estatus"]."'
                    WHERE id = '".$_POST["user_id"]."' ";
            }
            
            
            $consultaEditar = $this->db->query($query_editar);
            // $respuestaEditar = $consultaEditar->fetch_all(MYSQLI_ASSOC);
            if(isset($consultaEditar))
            {
                echo 'Usuario Editado';
            }
        }

        if($_POST['btn_action'] == 'delete'){
        
            $status = 'Activo';
            if($_POST['status'] == 'Activo'){
                
                $status = 'Inactivo';
            }
            $query_estatus = "
                update usuario
                set estatus = '".$status."'
                where id = '".$_POST["user_id"]."'
            ";
                
            $result = $this->db->query($query_estatus);
            if(isset($result)){
                
                echo 'Estatus Cambiado a ' . $status;
            }
        }

        // condicion para eliminar la fila seleccionada
        if($_POST['btn_action'] == 'borrar'){
        
            $query_delete = "
                delete  from usuario
                where id = '".$_POST["user_id"]."'
            ";
                
            $result = $this->db->query($query_delete);
            if(isset($result)){
                
                echo 'Datos Eliminados ';
            }
        }

    }
    //fin Modal  /insertar/editar/borrar usuario

    // metodo para obtener todos los Usuarios
    public function getAllUsuarios() {

        $query = '';

        $output = array();

        $query .= " SELECT * FROM usuario ";
        if(isset($_POST["search"]["value"]))
        {
            $query .= 'where correo_name LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR user_name LIKE "%'.$_POST["search"]["value"].'%" ';
        }
        if(isset($_POST["order"]))
        {
            $query .= 'ORDER by '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
        }
        else{
            $query .= 'ORDER BY id DESC ';
        }
        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT ' .$_POST['start'] . ', ' . $_POST['length'];
        }


        //ejecutamos la consulta
        $consulta = $this->db->query($query);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        $data = array();
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $filtered_rows = count($respuesta);
        foreach($respuesta as $row)
        {
            $status = '';
            if($row['estatus'] == 'Activo')
            {
                $status = '<span class="label label-success">Activo</span>';
            }
            else
            {
                $status = '<span class="label label-danger">Inactivo</span>';
            }
            $sub_array = array();
            $sub_array[] = $row['id'];
            $sub_array[] = $row['correo_name'];
            $sub_array[] = $row['user_name'];
            $sub_array[] = $row['user_type'];
            $sub_array[] = $status;
            $sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-xs update">Actualizar</button>';
            $sub_array[] = '<button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger btn-xs delete" data-status="'.$row["estatus"].'">Cambiar Estatus</button>';
            $sub_array[] = '<button type="button" name="borrar" id="'.$row["id"].'" class="btn btn-danger btn-xs borrar">Eliminar Usuario</button>';
            $data[]      = $sub_array;
        }
        
        $tabla = 'usuario';
        $output = array(
            "draw"              => intval($_POST["draw"]),
            "recordsTotal"      => $filtered_rows,
            "recordsFiltered"   => self::get_total_comentarios($tabla),
            "data"              => $data
        );

        return json_encode($output);
        //cierro consulta para que no quede en memoria
        $respuesta->close();
        // cierro conexion a la bd
        $this->db->close();
        
        // if($respuesta) {
            
        //     return $respuesta;
        //     //cierro consulta para que no quede en memoria
        //     $respuesta->close();
        //     // cierro conexion a la bd
        //     $this->db->close();
        // }
    }
    // fin metodo para los usuarios


    // metodos para perfil
    public function EditPerfil()
    {
        if(isset($_POST['user_name']))
        {
            if($_POST["user_new_password"] != '')
            {
                $query = "
                UPDATE usuario SET 
                user_name = '".$_POST["user_name"]."', 
                correo_name = '".$_POST["user_email"]."', 
                user_password = '".password_hash($_POST["user_new_password"], PASSWORD_DEFAULT)."' 
                WHERE id = '".$_POST["user_id2"]."'
                ";
            }
            else
            {
                $query = "
                UPDATE usuario SET 
                user_name = '".$_POST["user_name"]."', 
                correo_name = '".$_POST["user_email"]."'
                WHERE id = '".$_POST["user_id2"]."'
                ";
            }
             //ejecutamos la consulta
             $consulta = $this->db->query($query);
            
            if(isset($consulta))
            {
                echo '<div class="alert alert-success">Perfil Editado</div>';
            }
        }
        else
        {
            echo '<div class="alert alert-success">Ingrese un Nombre de Usuario</div>';
        }
    }
    // fin metodos para perfil
    // metodo para login al sistema
    public function getUser($userName) {
        $sql = "SELECT * FROM usuario WHERE correo_name='".$userName."'";
        $busca = $this->db->query($sql);
        // Obtener todas las filas en un array asociativo
        $respuesta = $busca->fetch_all(MYSQLI_ASSOC);
        if($respuesta) {
            
            return $respuesta;
            
        }
    }

    public function get_total_comentarios($tabla)
    {
        $comentarios = '';
        $comentarios = " SELECT * FROM $tabla";
        //ejecutamos la consulta
        $consulta = $this->db->query($comentarios);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $totalRows = count($respuesta);
        return $totalRows;

        //cierro consulta para que no quede en memoria
        $respuesta->close();
        // cierro conexion a la bd
        $this->db->close();
    }

}
?>