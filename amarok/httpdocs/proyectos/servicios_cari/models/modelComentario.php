<?php

require_once ('db.php');
require_once ('funciones.php');

class modelComentario{

    public function __construct(){
        $this->db=Conectar::conexion();
    }

    public function getDB(){
        return $this->db;
    }
    //mostrar comentarios en el front-end
    public function getComentario(){
        //hacemos una consulta
        $nombre = '';
        $comentario = '';
        $consulta=$this->db->query("SELECT * FROM comentario ORDER BY id DESC");
        foreach($consulta as $row){
            if($row['estatus'] == 'Activo')
            {
                $nombre = $row['nombre'];
                $comentario = $row['comentarios'];
                echo '<li class"col-md-12 comentarios-lista"><strong>Nombre: </strong>'.$nombre.'<br>'.$comentario.'<br><i class="far fa-smile icon-comentarios"></i></li><br>';    
            }
            
        }
        //cierro consulta para que no quede en memoria
        $consulta->close();
        // cierro conexion a la bd
        $this->db->close();
    
    }
    
    // metodo para obtener todos los comentarios en el banck-end
    public function getAllComentarios() {

        $query = '';

        $output = array();

        $query .= " SELECT * FROM comentario ";
        if(isset($_POST["search"]["value"]))
        {
            $query .= 'where email LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR nombre LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR telefono LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR email LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR comentarios LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR estatus LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR ip LIKE "%'.$_POST["search"]["value"].'%" ';
        }
        if(isset($_POST["order"]))
        {
            $query .= 'ORDER by '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
        }
        else{
            $query .= 'ORDER BY id DESC ';
        }
        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT ' .$_POST['start'] . ', ' . $_POST['length'];
        }


        //ejecutamos la consulta
        $consulta = $this->db->query($query);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        $data = array();
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $filtered_rows = count($respuesta);
        foreach($respuesta as $row)
        {
            $status = '';
            if($row['estatus'] == 'Activo')
            {
                $status = '<span class="label label-success">Activo</span>';
            }
            else
            {
                $status = '<span class="label label-danger">Inactivo</span>';
            }
            $sub_array = array();
            $sub_array[] = $row['id'];
            $sub_array[] = $row['nombre'];
            $sub_array[] = $row['telefono'];
            $sub_array[] = $row['email'];
            $sub_array[] = $row['comentarios'];
            $sub_array[] = $status;
            $sub_array[] = $row['ip'];
            $sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-xs update">Actualizar</button>';
            $sub_array[] = '<button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger btn-xs delete" data-status="'.$row["estatus"].'">Cambiar Estatus</button>';
            $sub_array[] = '<button type="button" name="borrar" id="'.$row["id"].'" class="btn btn-danger btn-xs borrar">Eliminar Comentario</button>';
            $data[]      = $sub_array;
        }
        
        $tabla = 'comentario';
        $output = array(
            "draw"              => intval($_POST["draw"]),
            "recordsTotal"      => $filtered_rows,
            "recordsFiltered"   => self::get_total_comentarios($tabla),
            "data"              => $data
        );

        return json_encode($output);
        
    }

    //inicio Modal editar comentarios
    public function ModalEditComentarios() {
        $query = '';
        $query_editar = '';
        $output = array();
        //si viene vacio es porque agrega un nuevo usuario
        if($_POST['btn_action'] == '')
        {
            
            $query_agregar = "
            INSERT INTO comentario (nombre, telefono, email, comentarios, estatus, ip) 
            VALUES ('".$_POST["user_name"]."', '".$_POST["user_telefono"]."' ,'".$_POST["user_email"]."', 
                    '".$_POST["user_textarea"]."', '".$_POST["user_estatus"]."', '".$_POST["user_ip"]."' );
            "; 
            $consultaAgregar = $this->db->query($query_agregar);
            
            if(isset($consultaAgregar))
            {
            echo 'Nuevo Comentario Agregado '.$consultaAgregar;
            }
        }

        if($_POST['btn_action'] == 'fetch_single')
        {
            
            $query = "SELECT * FROM comentario WHERE id ='".$_POST['user_id']."'";
            
            //ejecutamos la consulta
            $consulta = $this->db->query($query);
            $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
            
            foreach($respuesta as $row)
            {
                $output['user_name'] = $row['nombre'];
                $output['user_telefono'] = $row['telefono'];
                $output['user_email'] = $row['email'];
                $output['user_textarea'] = $row['comentarios'];
                $output['user_estatus'] = $row['estatus'];
                $output['user_ip']      = $row['ip'];
            }
            
            echo json_encode($output);
            
        }
        
        if($_POST['btn_action'] == 'Edit')
        {
            
            $query_editar = "UPDATE comentario SET nombre = '".$_POST["user_name"]."',
                    telefono = '".$_POST["user_telefono"]."', email = '".$_POST["user_email"]."', 
                    comentarios = '".$_POST["user_textarea"]."', estatus = '".$_POST["estatus"]."'
                    , ip = '".$_POST["user_ip"]."'
                    WHERE id = '".$_POST["user_id"]."' ";
            $consultaEditar = $this->db->query($query_editar);
            // $respuestaEditar = $consultaEditar->fetch_all(MYSQLI_ASSOC);
            if(isset($consultaEditar))
            {
                echo 'Comentario Editado '.$consultaEditar;
            }
        }

        if($_POST['btn_action'] == 'delete'){
        
            $status = 'Activo';
            if($_POST['status'] == 'Activo'){
                
                $status = 'Inactivo';
            }
            $query_estatus = "
                update comentario
                set estatus = '".$status."'
                where id = '".$_POST["user_id"]."'
            ";
                
            $result = $this->db->query($query_estatus);
            if(isset($result)){
                
                echo 'Estatus Cambiado a ' . $status;
            }
        }

        // condicion para eliminar la fila seleccionada
        if($_POST['btn_action'] == 'borrar'){
        
            $query_delete = "
                delete  from comentario
                where id = '".$_POST["user_id"]."'
            ";
                
            $result = $this->db->query($query_delete);
            if(isset($result)){
                
                echo 'Datos Eliminados ';
            }
        }

    }
    //fin Modal editar comentarios

    public function get_total_comentarios($tabla)
    {
        $comentarios = '';
        $comentarios = " SELECT * FROM $tabla";
        //ejecutamos la consulta
        $consulta = $this->db->query($comentarios);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $totalRows = count($respuesta);
        return $totalRows;
    }

    public function insertComentario() {
            $funcion = new funciones();

            $message = '';
            $name = '';
            $tel = '';
            $email = '';
            $mensaje = '';

            $email = $_POST["con_email"];
            $name = $_POST["con_name"];
            $tel = $_POST["con_tel"];
            $mensaje = $_POST["con_mensaje"];
            $email_clear = $funcion->clean_text($email);;
            $name_clear =  $funcion->clean_text($name);
            $mensaje_clear =  $funcion->clean_text($mensaje);

            if(isset($email_clear) && $email_clear != '' && $funcion->check_email($email_clear))
            {
                if(isset($name_clear) && $name_clear != '')
                {
                    if(isset($mensaje_clear) && $mensaje_clear != '')
                    {
                        $ip = $_SERVER["REMOTE_ADDR"];
                        $status = 'Inactivo';
                        $query = '';
                        $query = "
                        INSERT INTO comentario (nombre, telefono, email, comentarios, estatus, ip)
                        VALUES ('".$name_clear."', '".$tel."', '".$email_clear."', 
                                '".$mensaje_clear."', '".$status."', '".$ip."' );
                        "; 
                        $consultaAgregar = $this->db->query($query);
                        
                        if(isset($consultaAgregar) && $consultaAgregar != '')
                        {
                            $message_bd = 'Nuevo Usuario Agregado en la Base de Datos';
                        }
                        else
                        {
                            $message_bd = 'El Usuario no se pudo Agregar a la Base de Datos';
                        }

                        //    $path = 'upload/' . $_FILES["resume"]["name"];
                        //    move_uploaded_file($_FILES["resume"]["tmp_name"], $path);           
                        $message = '
                            <h3 align="center">Información del Cliente</h3>
                            <table border="1" width="100%" cellpadding="5" cellspacing="5">
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Nombre</td>
                                    <td style="padding-left:10px;" width="70%">'.$name_clear.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Telefono</td>
                                    <td style="padding-left:10px;" width="70%">'.$tel.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Email</td>
                                    <td style="padding-left:10px;" width="70%">'.$email_clear.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Comentario</td>
                                    <td style="padding-left:10px;" width="70%">'.$mensaje_clear.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Estatus</td>
                                    <td style="padding-left:10px;" width="70%">'.$status.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Ip</td>
                                    <td style="padding-left:10px;" width="70%">'.$ip.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Mensaje Base de Datos</td>
                                    <td style="padding-left:10px;" width="70%">'.$message_bd.'</td>
                                </tr>
                            </table>
                        ';

                        $message_muestra = '
                            <h3 align="center">Información del Cliente</h3>
                            <table border="1" width="100%" cellpadding="5" cellspacing="5">
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Nombre</td>
                                    <td style="padding-left:10px;" width="70%">'.$name_clear.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Telefono</td>
                                    <td style="padding-left:10px;" width="70%">'.$tel.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Email</td>
                                    <td style="padding-left:10px;" width="70%">'.$email_clear.'</td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" width="30%">Comentario</td>
                                    <td style="padding-left:10px;" width="70%">'.$mensaje_clear.'</td>
                                </tr>
                            </table>
                        ';

                        require ("phpmailer/class.phpmailer.php");
                        require ("phpmailer/class.smtp.php");
                        $mail = new PHPMailer();
                        $mail->IsSMTP();	                            //Sets Mailer to send message using SMTP
                        $mail->SMTPAuth = true;							//Sets SMTP authentication. Utilizes the Username and Password variables
                        $mail->Port = '587';							//Sets the default SMTP server port	
                        $mail->SMTPSecure = 'tls';		                //Definmos la seguridad como TLS
                        $mail->SMTPAuth = true;                         //Usar autenticación SMTP
                        $mail->SMTPOptions = array(
                            'ssl' => array('verify_peer' => false,'verify_peer_name' => false,'allow_self_signed' => true)
                        );//opciones para "saltarse" comprobación de certificados (hace posible del envío desde localhost)
                        //Esto es para activar el modo depuración. En entorno de pruebas lo mejor es 2, en producción siempre 0
                        // 0 = off (producción)
                        // 1 = client messages
                        // 2 = client and server messages
                        $mail->SMTPDebug  = 0;
                        $mail->CharSet = 'UTF-8';
                        $mail->Host = 'smtp.gmail.com';
                        //Sets the SMTP hosts of your Email hosting, this for gmail	
                        //El puerto será el 465 ya que usamos encriptación TLS
                        //El puerto 587 es soportado por la mayoría de los servidores SMTP y es útil para conexiones no encriptadas (sin TLS)

                        
                        $mail->Username = 'pruebaamarok@gmail.com';		//Sets SMTP username
                        $mail->Password = 'amarok123';					//Sets SMTP password
                        $mail->setFrom('no-reply@servicioscari.cl', 'Nuevo Comentario');
                        $mail->From = $email_clear;					//Sets the From email address for the message
                        $mail->FromName = $name_clear;				//Sets the From name of the message
                        $mail->AddAddress('pruebaamarok@gmail.com', 'amarokdatacenter.cl');		//Adds a "To" address
                        $mail->AddAddress('contacto@servicioscari.cl', 'servicioscari.cl');	
                        //Para enviar un correo formateado en HTML lo cargamos con la siguiente función. Si no, puedes meterle directamente una cadena de texto.
                        $mail->MsgHTML($message);
                        //Y por si nos bloquean el contenido HTML (algunos correos lo hacen por seguridad) una versión alternativa en texto plano (también será válida para lectores de pantalla)
                        $mail->AltBody = $message;
                        $mail->WordWrap = 50;							//Sets word wrapping on the body of the message to a given number of characters
                        $mail->IsHTML(true);							//Sets message type to HTML
                //    	$mail->AddAttachment($path);					//Adds an attachment from a path on the filesystem
                        $mail->Subject = 'Nuevo comentario';			//Sets the Subject of the message
                        $mail->Body = $message;							//An HTML or plain text message body
                        if($mail->Send())								//Send an Email. Return true on success or false on error
                        {
                            
                            echo $message = '<div class="alert alert-success text-center">Email enviado Exitosamente.</div><hr><div class="alert alert-info">'.$message_muestra.'</div>';
                            // echo $json;
                            
                //            unlink($path);
                        }
                        else
                        {
                            echo $message = '<div class="alert alert-danger text-center">Error al intentar enviar el email</div>';
                        }
                    }
                    else
                    {
                        echo $message = '<div class="alert alert-danger text-center">Escriba un Mensaje </div>';
                    }
                }
                else
                {
                    echo $message = '<div class="alert alert-danger text-center">Escriba un Nombre </div>';
                }
            }
            else
            {
                echo $message = '<div class="alert alert-danger text-center">Escriba un Email Valido</div>';
            }

        }
}