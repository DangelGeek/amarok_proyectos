<?php

require_once ('db.php');
require_once ('funciones.php');
 
class modelTelefono {

    public function __construct() {
        $this->db = Conectar::conexion();
    }
    public function getDB() {
        return $this->db;
    }

    // metodos para tabla telefonos
    // metodo para mostrar en el front-end del index los telefonos activos
    public function getTelefonos(){
        //hacemos una consulta
        $tel = '';
        $ws = '';
        $email = '';
        $consulta=$this->db->query("SELECT * FROM telefonos ORDER BY id DESC");
        foreach($consulta as $row){
            if($row['estatus'] == 'Activo')
            {
                $tel = $row['tel'];
                $ws = $row['ws'];
                $email = $row['email'];
                echo '<li><a class="link-default" href="tel:#">'.$tel.'</a></li><li><a class="link-default" href="tel:#">'.$ws.'</a></li>';
                // echo '<i style="margin-left:5px;" class="fa fa-phone" aria-hidden="true"></i>'.$tel.' / <i style="margin-left:5px;" class="fab fa-whatsapp" aria-hidden="true"></i>'.$ws;
            }
            
        }
        //cierro consulta para que no quede en memoria
        $consulta->close();
        // cierro conexion a la bd
        $this->db->close();
    // return  "<li>".$comentario."</li>";    
    
    }

    //inicio Modal editar Telefonos
    public function ModalEditTelefonos() {
        $query = '';
        $query_editar = '';
        $output = array();
        //si viene vacio es porque agrega un nuevo usuario
        if($_POST['btn_action2'] == '')
        {
            $query_agregar = "
            INSERT INTO telefonos (tel, ws, nombre, email, comentario, estatus) 
            VALUES ('".$_POST["user_tel"]."', '".$_POST["user_ws"]."', '".$_POST["user_name2"]."' ,'".$_POST["user_email2"]."', 
                    '".$_POST["user_textarea2"]."', 'Inactivo' );
            "; 
            $consultaAgregar = $this->db->query($query_agregar);
            
            if(isset($consultaAgregar))
            {
            echo 'Nuevo Télefono Agregado';
            }
        }

        if($_POST['btn_action2'] == 'fetch_single')
        {
            
            $query = "SELECT * FROM telefonos WHERE id ='".$_POST['user_id2']."'";
            
            //ejecutamos la consulta
            $consulta = $this->db->query($query);
            $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
            
            //recorro en el foreach para mostrar los datos a editar en el form del modal y retorno como json
            foreach($respuesta as $row)
            {
                $output['user_tel'] = $row['tel'];
                $output['user_ws'] = $row['ws'];
                $output['user_email'] = $row['email'];
                $output['user_name'] = $row['nombre'];
                $output['user_textarea'] = $row['comentario'];
            }
            
            echo json_encode($output);   
            
        }
        
        if($_POST['btn_action2'] == 'Edit')
        {
            
            $query_editar = "UPDATE telefonos SET tel='".$_POST["user_tel"]."', ws='".$_POST["user_ws"]."', nombre = '".$_POST["user_name2"]."',
                    email = '".$_POST["user_email2"]."', comentario = '".$_POST["user_textarea2"]."'
                    WHERE id = '".$_POST["user_id2"]."' ";
            $consultaEditar = $this->db->query($query_editar);
            // $respuestaEditar = $consultaEditar->fetch_all(MYSQLI_ASSOC);
            if(isset($consultaEditar))
            {
                echo 'Télefono Editado';
            }
        }

        if($_POST['btn_action2'] == 'delete2'){
        
            $status = 'Activo';
            if($_POST['status'] == 'Activo'){
                
                $status = 'Inactivo';
            }
            
            $query_estatus = "
                update telefonos
                set estatus = '".$status."'
                where id = '".$_POST["user_id2"]."'
            ";
                
            $result = $this->db->query($query_estatus);
            if(isset($result)){
                
                echo 'Estatus Cambiado a ' . $status;
            }
        }
        // condicion para eliminar la fila seleccionada
        if($_POST['btn_action2'] == 'delete3'){
        
            $query_delete = "
                delete  from telefonos
                where id = '".$_POST["user_id2"]."'
            ";
                
            $result = $this->db->query($query_delete);
            if(isset($result)){
                
                echo 'Datos Eliminados ';
            }
        }

    }
    //fin Modal editar Telefonos

    // metodo para obtener todos los telefonos y su demas campos
    public function getAllTelefonos() {

        $query = '';

        $output = array();

        $query .= " SELECT * FROM telefonos ";
        if(isset($_POST["search"]["value"]))
        {
            $query .= 'where tel LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR email LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR nombre LIKE "%'.$_POST["search"]["value"].'%" ';
        }
        if(isset($_POST["order"]))
        {
            $query .= 'ORDER by '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
        }
        else{
            $query .= 'ORDER BY id DESC ';
        }
        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT ' .$_POST['start'] . ', ' . $_POST['length'];
        }


        //ejecutamos la consulta
        $consulta = $this->db->query($query);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        $data = array();
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $filtered_rows = count($respuesta);
        foreach($respuesta as $row)
        {
            $status = '';
            if($row['estatus'] == 'Activo')
            {
                $status = '<span class="label label-success">Activo</span>';
            }
            else
            {
                $status = '<span class="label label-danger">Inactivo</span>';
            }
            $sub_array = array();
            $sub_array[] = $row['id'];
            $sub_array[] = $row['tel'];
            $sub_array[] = $row['ws'];
            $sub_array[] = $row['nombre'];
            $sub_array[] = $row['email'];
            $sub_array[] = $row['comentario'];
            $sub_array[] = $status;
            $sub_array[] = '<button type="button" name="update2" id="'.$row["id"].'" class="btn btn-warning btn-xs update2">Actualizar</button>';
            $sub_array[] = '<button type="button" name="delete2" id="'.$row["id"].'" class="btn btn-danger btn-xs delete2" data-status="'.$row["estatus"].'">Cambiar Estatus</button>';
            $sub_array[] = '<button type="button" name="delete3" id="'.$row["id"].'" class="btn btn-danger btn-xs delete3">Eliminar Datos</button>';
            $data[]      = $sub_array;
        }
        $tabla = 'telefonos';
        $output = array(
            "draw"              => intval($_POST["draw"]),
            "recordsTotal"      => $filtered_rows,
            "recordsFiltered"   => self::get_total_comentarios($tabla),
            "data"              => $data
        );

        return json_encode($output);
        //cierro consulta para que no quede en memoria
        $respuesta->close();
        // cierro conexion a la bd
        $this->db->close();
    }

    // fin metodos para tabla telefonos
    public function get_total_comentarios($tabla)
    {
        $comentarios = '';
        $comentarios = " SELECT * FROM $tabla";
        //ejecutamos la consulta
        $consulta = $this->db->query($comentarios);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $totalRows = count($respuesta);
        return $totalRows;

        //cierro consulta para que no quede en memoria
        $respuesta->close();
        // cierro conexion a la bd
        $this->db->close();
    }

}

?>