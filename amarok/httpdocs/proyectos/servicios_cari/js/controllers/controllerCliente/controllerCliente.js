

//muestra todos los datos de la tabla comentarios
$(document).ready(function() {
      
    $("#user_form").validate({
        
      rules         : {
          user_name      : { required : true, minlength: 2},
          user_apellido: { required:false, minlength: 2},
          user_telefono: { required:false, minlength: 7},
          user_email     : { required : true, email    : true},
          user_rut: { required:true, minlength: 3},
          user_comentario: { required:false, minlength:2},
          user_direccion: { required:false, minlength:2},
          user_comuna: { required:false, minlength:2}
      },
      messages      : {
          user_name       : "Debe introducir un nombre o empresa.",
          user_apellido   : "Debe introducir un apellido valido",
          user_telefono   : "Debe Introducir un télefono valido",
          user_email      : "Debe introducir un email válido.",
          user_rut        : "Debe introducir un rut valido.",
          user_comentario : "Ingrese más de 2 caracteres.",
          user_direccion  : "Ingrese una dirección valida",
          user_comuna     : "Ingrese una comuna valida"
      }
  });

      

  var userdataTable = $('#user_data').DataTable({
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax":{
      url:"../../controllers/controllerCliente/controllerGetClientes.php",
      type:"POST"
    },
    "columnDefs":[
      {
        "target":[4,12], //numero de columnas que se muestran, contadas desde 0
        "orderable":false
      }
    ],
    "pageLength":5 //maximo filas a mostrar en una vista
  });
  
  

  
  // cuando enviamos la data del formulario de cliente en el modal
  $(document).on('submit', '#user_form', function(event){
    event.preventDefault();
    
    $('#action').attr('disabled','disabled');
    var form_data = $(this).serialize();
    // console.log(form_data); 
    var tipo_user = $('#user_cliente').val();
    // console.log(' ' +tipo_user);
    if(tipo_user != 'Si' && tipo_user != 'No' )
    {
        $('#alert_user_cliente').fadeIn(1000).html('<div class="alert alert-danger">Solo se permite Si o No</div>').delay(1000).fadeOut(3000);
        return;
    }
    $.ajax({
     url:"../../controllers/controllerCliente/controllerEditCliente.php",
     method:"POST",
     data:form_data,
     success:function(data)
     {
       //el formulario esta enlazado con el evento click en la clase .update, cambiamos los valores de los inputos luego de editar a Add
      $('#btn_action').val('');
      $('#action').val('Add');
      $('.modal-title').html("<i class='fa fa-plus'></i> Agregar Cliente");
      $('#user_form')[0].reset();
      $('#userModal').modal('hide');
      $('#alert_action').fadeIn(1000).html('<div class="alert alert-success">'+data+'</div>').delay(1000).fadeOut(3000);
      $('#action').attr('disabled', false);
      userdataTable.ajax.reload();
     }
    })
   });

  // edita un usuario
  $(document).on('click', '.update', function(){
    var user_id = $(this).attr("id");
    var btn_action = 'fetch_single';
    $.ajax({
      url:"../../controllers/controllerCliente/controllerEditCliente.php",
      method:"POST",
      data:{
        user_id:user_id,
        btn_action:btn_action
      },
      dataType:"json",
      success:function(data)
      {
        //retorno la data en json, muestro el modal y los datos json en los inputs para editarlos
        console.log(data);
        $('#userModal').modal('show');
        $('#user_name').val(data.user_name);
        $('#user_apellido').val(data.user_apellido);
        $('#user_telefono').val(data.user_telefono);
        $('#user_email').val(data.user_email);
        $('#user_rut').val(data.user_rut);
        $('#user_comentario').val(data.user_comentario);
        $('#user_cliente').val(data.user_cliente);
        $('#user_direccion').val(data.user_direccion);
        $('#user_comuna').val(data.user_comuna);
        $('.modal-title').html("<i class='fa fa-pencil-square-o'></i>Editar Usuario");
        $('#user_id').val(user_id);
        $('#action').val('Edit');
        $('#btn_action').val('Edit');
      }
    });
  });
  //cambiar estatus
  $(document).on('click', '.cambiar', function(){
    var user_id = $(this).attr("id");
    var status = $(this).data('status');
    var btn_action = "cambiar";
    if(confirm("¿Seguro quieres cambiar el estatus?")){
            $.ajax({
                url:"../../controllers/controllerCliente/controllerEditCliente.php",
                method:"POST",
                data:{user_id:user_id, status:status,
                    btn_action:btn_action},
                success:function(data){
                    $("#alert_action").fadeIn(1000).html('<div class="alert alert-info">'+data+'</div>').delay(1000).fadeOut(3000);
                    userdataTable.ajax.reload();
                }
            })
    } else {
        return false;
    }
});

$(document).on('click', '.borrar', function(){
var user_id = $(this).attr("id");
var btn_action = "borrar";
if(confirm("¿Seguro quieres Eliminar el Usuario?")){
        $.ajax({
            url:"../../controllers/controllerCliente/controllerEditCliente.php",
            method:"POST",
            data:{user_id:user_id,
                btn_action:btn_action},
            success:function(data){
                $("#alert_action").fadeIn(1000).html('<div class="alert alert-info">'+data+'</div>').delay(1000).fadeOut(3000);
                userdataTable.ajax.reload();
            }
        })
} else {
    return false;
}
});

});