var userFeed = new Instafeed({
    get: 'user',
    userId: '8083601540',
    clientId: 'ec27919772f346069fac721e49f78baa',
    accessToken: '8083601540.1677ed0.845d43ef922e47db9d61b29263ba7bc7',
    resolution: 'standard_resolution',
    template: '<div class="col-xs-12 col-sm-6 col-md-4"><a href="{{image}}"><div class="img-featured-container"><div class="img-backdrop"></div><div class="description-container"><p class="caption">{{caption}}</p><span class="likes"><i class="fas fa-heart"></i> {{likes}}</span><span class="comments"><i class="fas fa-comment fa-flip-horizontal comentario"></i> {{comments}}</span></div><img src="{{image}}" class="img-responsive"></div></a></div>',
    sortBy: 'least-recent',
    limit: 6,
    links: false,
    after: function() {
        // Desabilita el boton si no existen más fotos para cargar
        if (!this.hasNext()) {
        btnInstafeedLoad.setAttribute('disabled', 'disabled');
        }
    }
  });
  //para saber si cargo el loader y se agrego la clase loaded y no den problemas las imagenes de instagram al hacerles zoom
  if($("div").find(".loaded")){
    setTimeout(function(){
        userFeed.run();
      }, 1000);    
  }
  

var btnInstafeedLoad = document.getElementById("btn-instafeed-load");
    btnInstafeedLoad.addEventListener("click", function() {
        userFeed.next()
    });
  

  // This will create a single gallery from all elements that have class "gallery-item"
  $('.gallery').magnificPopup({
    type: 'image',
    mainClass: 'mfp-with-zoom',
    delegate: 'a',
    zoom: {
        enabled: true, // By default it's false, so don't forget to enable it
        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out' // CSS transition easing function
    },
    gallery: {
        enabled: true
    }
});