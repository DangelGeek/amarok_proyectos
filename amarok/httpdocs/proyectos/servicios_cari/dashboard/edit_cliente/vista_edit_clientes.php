

<span id="alert_action"></span>
  <div class="row">
   <div class="col-lg-12">
    <div class="panel panel-default">
                    <div class="panel-heading">
                     <div class="row">
                         <div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
                             <h3 class="panel-title">Lista de Clientes</h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6" align="right">
                             <button type="button" name="add" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-success btn-xs">Add</button>
                         </div>
                        </div>
                       
                    <div class="clear:both"></div>
                    </div>
                    <div class="panel-body">
                     <div class="row">
                     <div class="col-sm-12 table-responsive">
                      <table id="user_data" class="table table-bordered table-striped">
                       <thead>
                        <tr>
                        <th readonly>ID</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Télefono</th>
                        <th>Email</th>
                        <th>RUT</th>
                        <th>Comentario</th>
                        <th>Cliente</th>
                        <th>Dirección</th>
                        <th>Comuna</th>
                        <th>Editar</th>
                        <th style="width: 167px;">Cambiar Estatus</th>
                        <th style="width: 147px;">Eliminar</th>
                        </tr>
                        </thead>
                      </table>
                     </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="userModal" class="modal fade">
            <div class="modal-dialog">
                <form method="post" id="user_form">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <i class="fa fa-plus"></i> Agregar Cliente</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="user_name">Ingrese Nombre</label>
                                <input type="text" name="user_name" id="user_name" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label for="user_apellido">Ingrese Apellido</label>
                                <input type="text" name="user_apellido" id="user_apellido" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="user_telefono">Ingrese Télefono</label>
                                <input type="text" name="user_telefono" id="user_telefono" class="form-control"  />
                            </div>
                            <div class="form-group">
                                <label for="user_email">Ingrese Email</label>
                                <input type="email" name="user_email" id="user_email" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label for="user_rut">Ingrese Rut</label>
                                <input type="text" name="user_rut" id="user_rut" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label for="user_comentario">Ingrese Comentario</label>
                                <textarea name="user_comentario" id="user_comentario" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="user_cliente">Es Cliente</label>
                                <select name="user_cliente" id="user_cliente" class="form-control" required>
                                    <option value='Si'>Si</option>
                                    <option value'No'>No</option>
                                </select>
                                <spam id="alert_user_cliente"></spam>
                            </div>
                            <div class="form-group">
                                <label for="user_direccion">Ingrese Dirección</label>
                                <input type="text" name="user_direccion" id="user_direccion" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="user_comuna">Ingrese Comuna</label>
                                <input type="text" name="user_comuna" id="user_comuna" class="form-control" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="user_id" id="user_id" />
                            <input type="hidden" name="btn_action" id="btn_action" />
                            <input type="submit" name="action" id="action" class="btn btn-info" value="Add" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>

