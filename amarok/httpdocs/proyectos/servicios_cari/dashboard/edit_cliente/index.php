<?php
    //index.php

    require_once ("../../models/modelLogin.php");

    // si no existe una session o no es master rediricciona al login
    $user = new modelLogin();
    if(!isset($_SESSION["type"]) || $_SESSION["type"] != 'master' )
    {
      
        header("location:../../ingreso");
    }

    
    // obtenemos la conexion con la bd
    $db= $user->getDB();
    
    $query = "
            SELECT * FROM usuario 
            WHERE id = '".$_SESSION["id"]."'
            ";
            //ejecutamos la consulta
            $consulta = $db->query($query);
            $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
            
            
            $name = '';
            $email = '';
            $user_id = '';
            foreach($respuesta as $row)
            {
            $name = $row['user_name'];
            $email = $row['correo_name'];
            }
            $user_id = $_SESSION["id"];

?>

   <!DOCTYPE html>
<html>
  <head>
    <title>Editar Clientes - Responsive Admin Dashboard</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content="Editar Clientes - Responsive Admin Dashboard" />
    <meta name="keywords" content="Empresa de Limpieza Industrial enfocada en lavado y limpieza de alfombras | lavado de tapices | lavado de sillones | decapado | cualquier parte de Chile." />
    <meta name="author" content="Luis Ponce" />
    
    <link rel="shortcut icon" href="../../images/lavado_tapiz.ico">
    <link href="../../recursos_sistema/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" /> -->
    
    <link href="../../recursos_sistema/css/new.css" rel="stylesheet">
    <!-- Important. For Theming change primary-color variable in main.css  -->

    <link href="../../recursos_sistema/fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
    
    <!-- Data Tables -->
		<link rel="stylesheet" href="../../recursos_sistema/css/datatables/dataTables.bs.min.css">
		
    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

    <script>
      // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      // })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');
      // ga('create', 'UA-40304444-1', 'iamsrinu.com');
      // ga('send', 'pageview');

    </script>
  </head>

  <body>

    <!-- Header Start -->
    <header>
      <a href="" class="logo">
        <img src="../../images/lavado_tapiz.png" alt="Logo"/ style="height:50px;">
      </a>
      <div class="pull-right">
        <ul id="mini-nav" class="clearfix">
          <li class="list-box user-profile">
            <a id="drop7" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
              <!-- <img src="img/user5.png" alt="Bluemoon User"> -->
              <h5>  <?php echo $_SESSION['user_name'] ?></h5>
            </a>
            <ul class="dropdown-menu server-activity">
              <li>
                <a  href="../edit_perfil/#" style="color:black;"><p><i class="fa fa-cog text-info"></i>  Configurar cuenta</p></a>
              </li>
              <li>
                <div class="demo-btn-group clearfix">
                  
                  <a href="../logout.php"><button class="btn btn-danger">
                    Salir
                  </button></a>
                  
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </header>
    <!-- Header End -->

    <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">
        <div class="contenedor_ivr card">
          <!-- Top Nav Start -->
          <div id='cssmenu'>
            <ul>
              <!-- <li class=''>
                  <a href=''><i class="fa fa-dashboard"></i>Dashboard</a>
                  <ul>
                    <li><a href=''>Estadísticas</a></li>
                  </ul>
              </li> -->
              <li class=''>
                <a href='../' ><i class="fa fa-dashboard"></i>Dashboard</a>
                <ul>
                  
                  <!-- <li><a href='#collapseVerCampana' data-toggle="collapse" aria-expanded="false" aria-controls="collapseVerCampana">Crear Comentarios</a></li> -->
                  <li><a href='../#collapseCrearCampana'>Ver Comentarios</a></li>
                  <li><a href='../#collapseTwo'>Mis Télefonos</a></li>
                 
                </ul>
              </li>
              <!-- modulo config inicio -->
              <li class=''>
              <a href='../edit_perfil'><i class="fa fa-dashboard"></i>Config</a>
              <ul>
                <li><a href='../edit_perfil#editarperfil' >Editar Perfil</a></li>
                <?php if($_SESSION['type'] == 'master')
                {
                ?>
                <li><a href='../edit_perfil#editarusuarios' >Editar Usuarios</a></li>
                <?php
                }
                ?>
              </ul>
              
            </li>
            
            <!-- modulo config fin -->
            <!-- modulo Editar clientes inicio -->
            <li class='active'>
              <a href='#'><i class="fa fa-dashboard"></i>Clientes</a>
              <ul>
                <li><a href='#editarcliente' data-toggle="collapse" aria-expanded="false" aria-controls="editarcliente">Editar Clientes</a></li>
              </ul>
            </li>
            <!-- modulo Editar clientes fin -->
            </ul>
          </div>
          <!-- Top Nav End -->

          <!-- Sub Nav End -->
          <div class="sub-nav hidden-sm hidden-xs">
            <ul>
              <li><a href='#editarcliente' data-toggle="collapse" aria-expanded="false" aria-controls="editarcliente" class="heading">Clientes</a></li>
              
              <li class="hidden-sm hidden-xs">
                <a href='#editarcliente' data-toggle="collapse" aria-expanded="false" aria-controls="editarcliente" >Editar Cliente</a>
                <!-- agregar class="selected" al tag a para activar -->
              </li>
            </ul>
           
          </div>
          <!-- Sub Nav End -->

          <!-- Dashboard Wrapper Start -->
          <div class="dashboard-wrapper">

            <!-- Row Start Editar Clientes -->
             <div id="editarcliente" class="row gutter collapse in" aria-labelledby="editarCliente" data-parent="#accordion">
              <div class="col-lg-12 col-md-12">
                <div class="widget no-margin">
                  <div class="widget-header">
                    <div class="title">
                      Editar Clientes
                    </div>
                    <span class="tools">
                      <i class="fa fa-cogs"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                    <div class="row gutter">
                      <?php include "vista_edit_clientes.php"; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End Editar Clientes -->
          </div>
          <!-- Dashboard Wrapper End -->
      </div>
        <footer>
          <p>© Amarok 2013-<?php echo date("y");?></p>
        </footer>

      </div>
    </div>
    <!-- Main Container end -->

    <script src="../../recursos_sistema/js/jquery.js"></script>
    <script src="../../recursos_sistema/js/bootstrap.min.js"></script>
    <script src="../../recursos_sistema/js/jquery.scrollUp.js"></script>

    <!-- Custom JS -->
    <script src="../../recursos_sistema/js/menu.js"></script>
    
    <script type="text/javascript">
      //ScrollUp
      $(function () {
        $.scrollUp({
          scrollName: 'scrollUp', // Element ID
          topDistance: '300', // Distance from top before showing element (px)
          topSpeed: 300, // Speed back to top (ms)
          animation: 'fade', // Fade, slide, none
          animationInSpeed: 400, // Animation in speed (ms)
          animationOutSpeed: 400, // Animation out speed (ms)
          scrollText: 'Top', // Text for element
          activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
      });

      //Tooltip
      $('a').tooltip('hide');

      //Popover
      $('.popover-pop').popover('hide');

      //Dropdown
      $('.dropdown-toggle').dropdown();

    </script>
    <!--jQuery validate para validar formulario en el front-end-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script> -->
    
    <script src="../../recursos_sistema/js/jquery-validate1.17.0.js"></script>
    <!-- <script src="correo_webservice/js/insertCorreo.js" type="text/javascript"></script> -->

    <!-- Data Tables -->
		<script src="../../recursos_sistema/js/datatables/dataTables.min.js"></script>
		<script src="../../recursos_sistema/js/datatables/dataTables.bootstrap.min.js"></script>

     <!-- controlador para la tabla de Usuarios -->
     <script src="../../js/controllers/controllerCliente/controllerCliente.js"></script>

  </body>
</html>