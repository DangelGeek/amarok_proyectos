$(document).ready(function(){

          $("#con_form").validate({
            
            rules         : {
                
                con_email     : { required : true, email    : true},
                con_name      : { required : true, minlength: 2},
                con_mensaje: { required:true, minlength: 2}
            },
            messages      : {
                con_email     : "Debe introducir un email válido.",
                con_name      : "Debe introducir su nombre.",
                con_mensaje : "El campo Mensaje es obligatorio."
                
            }
        });

//  cuando enviamos la data del formulario de usuario
    $(document).on('submit', '#con_form', function(event){
        event.preventDefault();
        $('#btn_submit').attr('disabled','disabled');
        // $('#recaptcha-container').html('');
        var form_data = $(this).serialize();
        $.ajax({
            url: "contacto/contacto.php",
            type: 'POST',
            data: form_data,
            success: function(data)
            {  
            if (data.length >= 100 ){
                $('#con_form')[0].reset();
                swal(
                "Datos del Email envíados!",
                "Gracias pronto te atenderemos!",
                "success"
                );
                $('#alert_cont').fadeIn(2000).html(data).delay(6000).fadeOut(3000);
                $('#btn_submit').attr('disabled', false);
                // $.getScript("https://www.google.com/recaptcha/api.js");
            } 
            else {
                $('#con_form')[0].reset();
                swal(
                "Error!",
                "Intente nuevamente!",
                "error"
                );
                $('#alert_cont').fadeIn(2000).html(data).delay(6000).fadeOut(3000);
                // $.getScript("https://www.google.com/recaptcha/api.js");
                $('#btn_submit').attr('disabled', false);
                }
            } 
        });
        
        });

    
        
});