<!DOCTYPE html>
<html>
	<head>
		<title>Comprobar si existe el email enviado con php, ajax,jquery </title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<br />
		<div class="container">
			<div class="row">	
				<div class="col-md-8" style="margin:0 auto; float:none;">
					<h3 align="center">Validar si existe email</h3>
					<br />
					<form method="post">
						<div align="center" class="row">
						<div id="respuesta" class="col-md-12">
						</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Ingrese Dirección de email</label>
									<input type="email" name="email" id="email" class="form-control" placeholder="Dirección de email" />
								</div>
							</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group" align="center">
								<input type="submit" name="submit" value="Submit" class="btn btn-info"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
	<script src="controlador/validaremail.js"></script>
</html>