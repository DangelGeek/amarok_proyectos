$(function(){

	$('form').on('submit',function(){
		var email = $('#email').val();
		console.log(email);
		if(email.length > 0){
			$.ajax({
				type: 'POST',	
				url: 'modelo/validaremail.php',
            	data: 'email='+email,
				beforeSend : function (){	
					$('#respuesta').html('<center><img src="recursos/cargando.gif" width="50" heigh="50"></center>').show(300);	
				},
				success: function(data){
					if(data == 'OK'){
						$('#respuesta').html('<div class="alert alert-success">El correo '+email+' Existe</div>').show(300).delay(3000).hide(300);
						return false;	
					}else{
						$('#respuesta').html('<div class="alert alert-danger">'+data+'</div>').show(300).delay(3000).hide(300);
						return false;
					}			
				}	
			});
			return false;	
		}else{
			$('#respuesta').html('<div class="alert alert-danger text-center">Ingrese un email</div>').show(300).delay(3000).hide(300);
			return false;	
		}
	});

});
